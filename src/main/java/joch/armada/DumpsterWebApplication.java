package joch.armada;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author ic
 */
@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
@Configuration
public class DumpsterWebApplication
{
    public static void main(String[] args) throws Exception
    {
        SpringApplication.run(DumpsterWebApplication.class, args);        
    }      
    
}
