 package joch.armada.api.interceptor;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ic
 */
@Component
public class HttpRequestInterceptor implements HandlerInterceptor
{    
    private final Logger logger = LoggerFactory.getLogger(HttpRequestInterceptor.class);            
    
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse response, Object o) throws Exception
    {   
        Map<String, String[]> queryParams = httpServletRequest.getParameterMap();
        String servletPath = httpServletRequest.getServletPath();
        final StringBuilder logMessage;
        logMessage = new StringBuilder("Request started: ")
        .append("[").append(httpServletRequest.getMethod()).append("]")
        .append(" [");
        if (servletPath.equals("/error") == false) {
            logMessage.append(getFullHttpRequestUrl(httpServletRequest));
        }
        else
        {
            logMessage.append(getHttpRequestUrlNoQueryString(httpServletRequest));
        }    
        //.append("] PATH: [")
        //.append(getHttpRequestUrlNoQueryString(httpServletRequest))
        //.append("]")
        //.append("] REQUEST PARAMETERS: ")
        //.append(queryParams);
        
        logMessage.append("]");        
        logger.info(logMessage.toString());              
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView mav) throws Exception {
        if(o != null)
        {            
            logger.info("Method executed: " + o);                 
        }           
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse response, Object o, Exception excptn) throws Exception
    {               
        final StringBuilder logMessage = new StringBuilder("Request ended: ");                                
        if (response.getStatus() != 404) {
            logMessage.append("[").append(getFullHttpRequestUrl(httpServletRequest));
            logMessage.append("]");
        }        
        logMessage.append(" with HTTP status ").append(response.getStatus());
        logger.info(logMessage.toString());               
    }
    
    private static String appendParams(Map<String, String[]> queryParams)
    {
        StringBuilder sb = new StringBuilder();
        queryParams.forEach(((String k, String[] v) -> {
            sb.append(k).append("=>").append(v);
            
            String[] values = v;
            ArrayList list = Lists.newArrayList(values);            
            list.stream().forEachOrdered((item)->
            {
                //sb.append(item).append(",");
            });
        }
        ));
        return sb.toString();
    }
    
    public static String getHttpRequestUrlNoQueryString(HttpServletRequest httpServletRequest)
    {
        String scheme = httpServletRequest.getScheme();
        String serverName = httpServletRequest.getServerName();
        int serverPort = httpServletRequest.getServerPort();
        String contextPath = httpServletRequest.getContextPath();
        String servletPath = httpServletRequest.getServletPath();
        //String pathInfo = httpServletRequest.getPathInfo();
        //String queryString = httpServletRequest.getQueryString();
        StringBuilder sb = new StringBuilder(scheme);
        sb.append("://")
                .append(serverName)
                .append(":")
                .append(serverPort)
                .append(contextPath)
                .append(servletPath);
        return sb.toString();
    }    
    
    public static String getFullHttpRequestUrl(HttpServletRequest httpServletRequest)
    {
        String scheme = httpServletRequest.getScheme();
        String serverName = httpServletRequest.getServerName();
        int serverPort = httpServletRequest.getServerPort();
        String contextPath = httpServletRequest.getContextPath();
        String servletPath = httpServletRequest.getServletPath();
        String pathInfo = httpServletRequest.getPathInfo();
        String queryString = httpServletRequest.getQueryString();
        StringBuilder sb = new StringBuilder(scheme);
        sb.append("://")
                .append(serverName)
                .append(":")
                .append(serverPort)
                .append(contextPath)
                .append(servletPath);
        
        if(queryString != null){
            sb.append("?" + queryString);
        }        
        return sb.toString();
    }
    
    
    /*
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception
    {   
        Map<String, String[]> queryParams = request.getParameterMap();
        final StringBuilder logMessage = new StringBuilder("Request started: ")
            .append("[")
            .append(request.getMethod())
            .append("] PATH: [")            
            .append(getHttpRequestUrlNoQueryString(httpServletRequest))            
            .append("] REQUEST PARAMETERS: ")
            .append(queryParams);
        logger.info(logMessage.toString());              
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView mav) throws Exception {
        if(o != null)
        {            
            logger.info("Method executed: " + o);                 
        }           
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception excptn) throws Exception
    {               
        final StringBuilder logMessage = new StringBuilder("Request ended: ")                        
            .append("[")            
            .append(getFullHttpRequestUrl(httpServletRequest))            
            .append("]")
            .append(" with HTTP status ").append(response.getStatus());            
        logger.info(logMessage.toString());                 
    }
    
    private static String getHttpRequestUrlNoQueryString(HttpServletRequest httpServletRequest)
    {
        String scheme = httpServletRequest.getScheme();
        String serverName = httpServletRequest.getServerName();
        int serverPort = httpServletRequest.getServerPort();
        String contextPath = httpServletRequest.getContextPath();
        String servletPath = httpServletRequest.getServletPath();
        //String pathInfo = httpServletRequest.getPathInfo();
        //String queryString = httpServletRequest.getQueryString();
        StringBuilder sb = new StringBuilder(scheme);
        sb.append("://")
                .append(serverName)
                .append(":")
                .append(serverPort)
                .append(contextPath)
                .append(servletPath);
        return sb.toString();
    }    
    
    private static String getFullHttpRequestUrl(HttpServletRequest httpServletRequest)
    {
        String scheme = httpServletRequest.getScheme();
        String serverName = httpServletRequest.getServerName();
        int serverPort = httpServletRequest.getServerPort();
        String contextPath = httpServletRequest.getContextPath();
        String servletPath = httpServletRequest.getServletPath();
        String pathInfo = httpServletRequest.getPathInfo();
        String queryString = httpServletRequest.getQueryString();
        StringBuilder sb = new StringBuilder(scheme);
        sb.append("://")
                .append(serverName)
                .append(":")
                .append(serverPort)
                .append(contextPath)
                .append(servletPath);
        
        if(queryString != null){
            sb.append("?" + queryString);
        }        
        return sb.toString();
    }
    
    */
    
}
