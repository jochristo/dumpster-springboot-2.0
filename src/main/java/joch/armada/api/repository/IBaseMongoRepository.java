package joch.armada.api.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Admin
 * @param <T>
 */
@Repository
public interface IBaseMongoRepository<T extends Object> extends QuerydslPredicateExecutor<T>
{

}
