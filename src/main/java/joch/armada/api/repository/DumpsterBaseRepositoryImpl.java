/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joch.armada.api.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import joch.armada.api.domain.Dumpster;
import joch.armada.data.mongo.predicate.EntityPredicateBuilder;
import org.joko.core.utilities.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ic
 */
@Component
public class DumpsterBaseRepositoryImpl
{    
    @Autowired
    private IDumpsterBaseRepository repository;

    public List<Dumpster> findWith(Map<Object,Object> requestMap)
    {
        if(Utilities.isEmpty(requestMap) == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Dumpster> predicate = new EntityPredicateBuilder(Dumpster.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){
                    predicate.with((String) key, ":", value);
                }                                
            }));     
            List<Dumpster> list = new ArrayList();
            Iterable<Dumpster> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null;       
    }
    
}
