package joch.armada.api.repository;

import java.util.Date;
import java.util.List;
import joch.armada.api.domain.Reservation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface IReservationRepository extends MongoRepository<Reservation, String>, QuerydslPredicateExecutor<Reservation>
{
    @Query(value = "{ 'id' : ?0 , 'deleted' : false }")
    //@Override    
    public Reservation findOne(String id);         
     
    @Query(value = "{ 'createdAt' : { $gte: ?0}, 'deleted' : false }")    
    public List<Reservation> findByDate(Date createdAt);         

    @Query(value = "{ 'id' : ?0 ,'deleted' : false }") 
    //@Override    
    public boolean exists(String id);    
    
    @Query(value = "{ 'customer.id' : ?0, 'customer.deleted' : false , 'deleted' : false }")     
    public boolean existsCustomerId(String customerId);      
    
    @Query(value = "{ 'deleted' : false }")    
    public List<Reservation> findUnDeleted();          
    
    @Query(value = "{ 'deleted' : true }")    
    public List<Reservation> findDeleted();  
    
    
}
