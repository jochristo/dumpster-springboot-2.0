package joch.armada.api.repository;

import java.util.Date;
import java.util.List;
import joch.armada.api.domain.Placement;
import joch.armada.api.domain.enumeration.PlacementStatus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface IPlacementRepository extends MongoRepository<Placement, String>, QuerydslPredicateExecutor<Placement>
{
    @Query(value = "{ 'id' : ?0 , 'deleted' : false }")
    //@Override
    public Placement findOne(String id);
    
    @Query(value = "{ 'status' : ?0 , 'deleted' : false }")
    public List<Placement> find(PlacementStatus status); 
    
    @Query(value = "{ 'longitude' : { $lt: ?1, $gt: ?0} , 'latitude' : { $lt: ?3, $gt: ?2} , 'deleted' : false }")    
    public List<Placement> find(double lonMin, double lonMax, double latMin, double latMax);    
    
    @Query(value = "{ 'longitude' : { $lt: ?1, $gt: ?0} , 'latitude' : { $lt: ?3, $gt: ?2} , 'deleted' : false }")    
    public List<Placement> findByDateIn(Date dateInFrom, Date dateInTo);     
    
    @Query(value = "{ 'dateOut' : {$gte : ?0}, 'deleted' : false }")    
    public List<Placement> findByDateOut(Date dateOut);

    @Query(value = "{ 'id' : ?0 ,'deleted' : false }") 
    //@Override
    public boolean exists(String id);    
    
    @Query(value = "{ 'dumpsterReferenceId' : ?0, 'deleted' : false }")     
    public boolean existsDumpsterReferenceId(String dumpsterReferenceId);          
    
    @Query(value = "{ 'deleted' : false }")    
    public List<Placement> findUndeleted();          
    
    @Query(value = "{ 'deleted' : true }")    
    public List<Placement> findDeleted();     
        
    
}
