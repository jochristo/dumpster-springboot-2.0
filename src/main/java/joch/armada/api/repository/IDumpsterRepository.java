package joch.armada.api.repository;

import joch.armada.api.domain.Dumpster;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface IDumpsterRepository extends MongoRepository<Dumpster, String>, QuerydslPredicateExecutor<Dumpster>
{
    
}
