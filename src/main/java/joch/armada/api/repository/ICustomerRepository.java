package joch.armada.api.repository;

import java.util.List;
import joch.armada.api.domain.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface ICustomerRepository extends MongoRepository<Customer,String>, QuerydslPredicateExecutor<Customer>
{
    @Query(value = "{ 'email' : ?0 , 'deleted' : false }")
    public Customer findByEmail(String email);
    
    @Query(value = "{ 'mobileNo' : ?0 , 'deleted' : false }")
    public Customer findByMobileNo(String mobileNo);
    
    @Query(value = "{ 'phoneNo' : ?0 , 'deleted' : false }")
    public Customer findByPhoneNo(String phoneNo);
    
    @Query(value = "{ 'lastname' : ?0 , 'deleted' : false }")
    public List<Customer> findByLastname(String lastname);

    @Query(value = "{ 'id' : ?0 , 'deleted' : false }")
    public Customer findOne(Object id);
    
    @Query(value = "{ 'deleted' : false }")
    public List<Customer> findUnDeleted(); 
    
    @Query(value = "{ 'deleted' : true }")
    public List<Customer> findDeleted();    

    @Query(value = "{ 'id' : ?0 , 'deleted' : false }")
    //@Override
    public boolean exists(String id);
    
    @Query(value = "{ 'email' : ?0 , 'deleted' : false }")    
    public boolean existsEmail(String email);    
    
    @Query(value = "{ 'mobileNo' : ?0 , 'deleted' : false }")    
    public boolean existsMobileNo(String mobileNo);
    
    
       
}
