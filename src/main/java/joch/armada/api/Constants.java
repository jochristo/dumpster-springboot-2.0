package joch.armada.api;

import org.springframework.http.MediaType;

/**
 *
 * @author ic
 */
public interface Constants {

    
    public final static String APPLICATION_JSON_UTF8_VALUE = MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8";    
    public final static MediaType APPLICATION_JSON_UTF8 = MediaType.valueOf(APPLICATION_JSON_UTF8_VALUE);         
    
    
    public interface JSONApiRelationshipName
    {
        public final static String CUSTOMERS_RELATIONSHIP = "customers";
        public final static String PLACEMENTS_RELATIONSHIP = "placements";
    }
    
    public interface EntityTypeName
    {
        public final static String CUSTOMERS = "customers";
        public final static String PLACEMENTS = "placements";
        public final static String DUMPSTERS = "dumpsters";
        public final static String RESERVATIONS = "reservations";
    }    
        
    /**
     * Error codes for application-specific errors.
     */
    public enum ErrorCode
    {                
        NOT_EMPTY_ATTRIBUTE(1001),
        NICKNAME_ATTRIBUTE(1002),
        INVALID_EMAIL_ERROR_CODE(1003),
        JSON_DATE_ATTRIBUTE(1004),        
        INVALID_JSON_FORMAT(1005),
        DUPLICATE_EMAIL(1006),
        DUPLICATE_NICKNAME(1007),
        MEMCACHED_TIMEOUT(1008),
        PASSWORD_MISMATCH(1009),
        INCORRECT_PASSWORD(1010),
        INVALID_LONGITUDE_ERROR_CODE(1011),
        INVALID_LATITUDE_ERROR_CODE(1012),
        INVALID_SCORE_ERROR_CODE(1013),
        INVALID_LEVEL_ERROR_CODE(1014),
        INVALID_MOBILE_NO_ERROR_CODE(1015),                
        INVALID_NUMBER_GENERAL(1016),
        DUPLICATE_ANSWERS(1017),
        INVALID_ANSWER_ATTRIBUTE_ERROR_CODE(1018),
        INVALID_IMAGE_FORMAT(1020),
        
        MASTERCARD_API_ERROR_CODE(3000),        
        ;        
        
        private ErrorCode(int value) { this.value = value; }
        private final int value;        
        public int getCode(){
            return this.value;
        }
        public String getCodeAsString() {
            return String.valueOf(this.value);
        }                
    }    
    
    public interface ErrorCodeMessage
    {
        //invalid attribute messages
        public static final String INVALID_LONGITUDE_MESSAGE = "Longitude must be between -180 and 180 degrees inclusive";    
        public static final String INVALID_LATITUDE_MESSAGE = "Latitude must be between -90 and 90 degrees inclusive";                        
        public static final String INVALID_RADIUS_MESSAGE = "Radius must be an unsigned integer value";         
        public static final String INVALID_MOBILE_PHONE_MESSAGE = "Mobile phone number  must be in the format (+30) xxxx xxx xxx";         
        public static final String INVALID_PHONE_MESSAGE = "Phone number  must be in numeric format";         
        public static final String INVALID_EMAIL_MESSAGE = "Email format must be some_user@domain, e.g. johndoe@johndoe.org";         
    }
    
    public interface ErrorCodeTitle
    {
        //invalid attribute titles
        public static final String INVALID_LONGITUDE_TITLE  = "Invalid longitude value";
        public static final String INVALID_LATITUDE_TITLE = "Invalid latitude value";        
        public static final String INVALID_RADIUS_TITLE  = "Invalid radius value";        
        public static final String INVALID_MOBILE_PHONE_TITLE  = "Invalid mobile phone number";       
        public static final String INVALID_PHONE_TITLE  = "Invalid phone number";    
        public static final String INVALID_EMAIL_TITLE  = "Invalid email format";   
        
        
    }
    
    
    
}
