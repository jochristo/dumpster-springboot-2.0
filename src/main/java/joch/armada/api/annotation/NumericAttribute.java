package joch.armada.api.annotation;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import joch.armada.api.Constants.ErrorCode;
import joch.armada.api.validator.NumericValidator;

/**
 *
 * @author ic
 */
@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Constraint(validatedBy = NumericValidator.class)
@Documented
@Retention(RUNTIME)
public @interface NumericAttribute {
    
    AttributeType pattern();
    String title() default "";
    String message() default "";
    String code() default "1011";
    ErrorCode errorCode() default ErrorCode.INVALID_NUMBER_GENERAL;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    
    public enum AttributeType
    {
        LONGITUDE(1),
        LATITUDE(2),
        MOBILE_NUMBER(3),        
        GENERAL_NUMBER(6);        
        private AttributeType(int value) { this.value = value; }
        private final int value;        
        public int getCode(){
            return this.value;
        }

    }      

}
