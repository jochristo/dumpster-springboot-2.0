package joch.armada.api.annotation;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import joch.armada.api.validator.PredicateOperationTypeValidator;

/**
 * Designates a QueryDSL operation type enumeration.
 * @author ic
 */
@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Constraint(validatedBy = PredicateOperationTypeValidator.class)
@Documented
@Retention(RUNTIME)
public @interface PredicateOperationTypeAttribute
{
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};     
    
}
