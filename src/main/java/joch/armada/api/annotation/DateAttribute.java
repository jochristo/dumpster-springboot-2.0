package joch.armada.api.annotation;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import joch.armada.api.validator.DateFormatValidator;

@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Constraint(validatedBy = DateFormatValidator.class)
@Documented
@Retention(RUNTIME)
public @interface DateAttribute {
    
    String message() default "Date format is invalid. Allowed formats: dd-MM-yyyy";
    String title() default "Invalid date format";
    String code() default "1004";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};     
    JsonDateFormatPattern pattern() default JsonDateFormatPattern.shortWithDash;
    
    public enum JsonDateFormatPattern
    {
        shortWithDash(1),
        shortWithSlash(2);      
        private JsonDateFormatPattern(int value) { this.value = value; }
        private final int value;
    }    
}
