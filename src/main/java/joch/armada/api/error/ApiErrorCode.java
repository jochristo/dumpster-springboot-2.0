package joch.armada.api.error;

import org.springframework.http.HttpStatus;

/**
 * Custom API error codes
 * @author ic
 */
public enum ApiErrorCode
{

    // API's ERROR COES
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "500", "INTERNAL_SERVER_ERROR"),
    
    CONSTRAINT_VIOLATION(HttpStatus.BAD_REQUEST, "1000", "CONSTRAINT_VIOLATION"),
    
    DATA_INTEGRITY_VIOLATION(HttpStatus.BAD_REQUEST, "1001", "DATA_INTEGRITY_VIOLATION"),

    RESOURCE_NOT_FOUND(HttpStatus.NOT_FOUND, "404", "RESOURCE_NOT_FOUND"),
    
    DATA_NOT_FOUND(HttpStatus.NOT_FOUND, "404", "DATA_NOT_FOUND"),
    
    MISSING_REQUEST_PARAMETER(HttpStatus.BAD_REQUEST, "2001", "MISSING_REQUEST_PARAMETER"),
    
    INVALID_PARAMETER_VALUE(HttpStatus.BAD_REQUEST, "2002", "INVALID_PARAMETER_VALUE"),
    
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED, "401", "UNAUTHORIZED"),

    INVALID_LOGIN_CREDENTIALS(HttpStatus.UNAUTHORIZED, "401", "INVALID_LOGIN_CREDENTIALS"),    
    ;   
    
    private final HttpStatus httpStatus;
    private final String appCode;
    private final String title;        
    private final int statusCode;
    
    public int getStatusCode()
    {
        return this.httpStatus.value();
    }
    
    public String getStatusAsString()
    {
        return String.valueOf(this.httpStatus.value());
    }    

    private ApiErrorCode(HttpStatus httpStatus, String appCode, String title)
    {
        this.httpStatus = httpStatus;
        this.appCode = appCode;
        this.title = title;
        this.statusCode = httpStatus.value();
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getAppCode() {
        return appCode;
    }

    public String getTitle() {
        return title;
    }
    
    
    
}
