package joch.armada.api.error;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.UUID;
import org.joko.core.jsonapi.annotations.JAPIAttribute;
import org.joko.core.jsonapi.annotations.JAPIIdentifier;
import org.joko.core.jsonapi.annotations.JAPIType;

/**
 * Describes application-specific errors : HTTP status, code, and specific details.
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JAPIType(type="appCodes")
public class ApplicationCode {
    
    @JAPIIdentifier
    @JsonIgnore
    private String id = UUID.randomUUID().toString();
    
    @JAPIAttribute
    private String status;    
    
    @JAPIAttribute
    private String code;
    
    @JAPIAttribute
    private String details;

    public ApplicationCode() {        
        
    }
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
        
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
    
    
}
