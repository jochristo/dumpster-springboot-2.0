package joch.armada.api.error;

import java.util.ArrayList;
import java.util.List;
import static joch.armada.api.error.ApiErrorCode.CONSTRAINT_VIOLATION;
import static joch.armada.api.error.ApiErrorCode.DATA_INTEGRITY_VIOLATION;
import static joch.armada.api.error.ApiErrorCode.INTERNAL_SERVER_ERROR;
import static joch.armada.api.error.ApiErrorCode.INVALID_LOGIN_CREDENTIALS;
import static joch.armada.api.error.ApiErrorCode.INVALID_PARAMETER_VALUE;
import static joch.armada.api.error.ApiErrorCode.MISSING_REQUEST_PARAMETER;
import static joch.armada.api.error.ApiErrorCode.RESOURCE_NOT_FOUND;
import static joch.armada.api.error.ApiErrorCode.UNAUTHORIZED;

/**
 *
 * @author ic
 */
public class Utils {
    
    /**
     * Gets a list of all application-specific error codes.
     * @return 
     */
    public static List<ApplicationCode> applicationCodes()
    {
        List<ApplicationCode> codes = new ArrayList();        
        
        ApplicationCode code = new ApplicationCode();
        code.setStatus(INTERNAL_SERVER_ERROR.getStatusAsString());
        code.setCode(INTERNAL_SERVER_ERROR.getAppCode());
        code.setDetails(INTERNAL_SERVER_ERROR.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(CONSTRAINT_VIOLATION.getStatusAsString());
        code.setCode(CONSTRAINT_VIOLATION.getAppCode());
        code.setDetails(CONSTRAINT_VIOLATION.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(RESOURCE_NOT_FOUND.getStatusAsString());
        code.setCode(RESOURCE_NOT_FOUND.getAppCode());
        code.setDetails(RESOURCE_NOT_FOUND.getTitle());
        codes.add(code);        
        
        code = new ApplicationCode();
        code.setStatus(MISSING_REQUEST_PARAMETER.getStatusAsString());
        code.setCode(MISSING_REQUEST_PARAMETER.getAppCode());
        code.setDetails(MISSING_REQUEST_PARAMETER.getTitle());
        codes.add(code); 
        
        code = new ApplicationCode();
        code.setStatus(INVALID_PARAMETER_VALUE.getStatusAsString());
        code.setCode(INVALID_PARAMETER_VALUE.getAppCode());
        code.setDetails(INVALID_PARAMETER_VALUE.getTitle());
        codes.add(code);
        
        
        code = new ApplicationCode();
        code.setStatus(DATA_INTEGRITY_VIOLATION.getStatusAsString());
        code.setCode(DATA_INTEGRITY_VIOLATION.getAppCode());
        code.setDetails(DATA_INTEGRITY_VIOLATION.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(INVALID_LOGIN_CREDENTIALS.getStatusAsString());
        code.setCode(INVALID_LOGIN_CREDENTIALS.getAppCode());
        code.setDetails(INVALID_LOGIN_CREDENTIALS.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(UNAUTHORIZED.getStatusAsString());
        code.setCode(UNAUTHORIZED.getAppCode());
        code.setDetails(UNAUTHORIZED.getTitle());
        codes.add(code);
        
        return codes;        
    }
}
