package joch.armada.api.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.api.exception.InvalidParameterArgumentException;
import org.joko.core.jsonapi.JSONApiDocument;
import org.joko.core.jsonapi.JSONApiErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import joch.armada.api.Endpoints;
import joch.armada.api.exception.ResourceNotFoundException;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.ServletWebRequest;

/**
 *
 * @author ic
 */
@RestController
public class SpringBootErrorController implements ErrorController {

    @Autowired
    private ErrorAttributes errorAttributes;

    private static final Logger logger = LoggerFactory.getLogger(SpringBootErrorController.class);

    /**
     * Handling the Errors occured.
     *
     * @param request	The Request produce the Errors
     * @param response	The Response for the Errors occured.
     * @return	JSONApiDocument
     */
    //@RequestMapping(value = Endpoints.SPRING_BOOT_ERRORS, method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT}, produces = "application/json")
    public JSONApiDocument error(HttpServletRequest request, HttpServletResponse response)
    {
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
        JSONApiErrors error = new JSONApiErrors();
        Map<String, Object> errorAttributes = getErrorAttributes(request, false);        
        String appCode = (String) request.getAttribute("appCode");
        String details = (String) request.getAttribute("details");
        String status = String.valueOf(errorAttributes.get("status"));
        String message = String.valueOf(errorAttributes.get("message"));
        //timestamp,status,error,exception,message,path

        switch (status) {

            case "403"://HttpStatus.METHOD_NOT_ALLOWED
                appCode = status;
                details = message;
                message = HttpStatus.FORBIDDEN.getReasonPhrase();
                break;

            case "404"://HttpStatus.NOT_FOUND
                appCode = status;
                details = message;
                message = HttpStatus.NOT_FOUND.getReasonPhrase();
                break;

            case "405"://HttpStatus.METHOD_NOT_ALLOWED
                appCode = status;
                details = message;
                message = HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase();
                break;

            case "500": //
                appCode = "500";
                details = "INTERNAL_SERVER_ERROR";
                message = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
                break;

            default:
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");
                break;

        }

        List<FieldError> fieldErrors = (List<FieldError>) errorAttributes.get("errors");        
        if (fieldErrors != null) {
            for (FieldError fe : fieldErrors) {
                JSONApiErrors errorz = new JSONApiErrors();
                errorz.setStatus(status);
                errorz.setCode((appCode == null) ? String.valueOf(errorAttributes.get("appCode")) : appCode);
                errorz.setTitle(message);
                errorz.setDetail("Invalid " + fe.getField());
                errorsList.add(errorz);
                //throw new RestApplicationException(HttpStatus.BAD_REQUEST, ApiError.ILLEGAL_ARGUMENT_TYPE, "Invalid " + fe.getField());                
                throw new InvalidParameterArgumentException(ApiErrorCode.INVALID_PARAMETER_VALUE, "Invalid " + fe.getField());                
            }

            //details = sb.toString();
        } else {

            error.setStatus(status);
            error.setCode((appCode == null) ? String.valueOf(errorAttributes.get("appCode")) : appCode);
            error.setTitle(message);
            error.setDetail((details == null) ? String.valueOf(errorAttributes.get("details")) : details);
            errorsList.add(error);
            //throw new RestApplicationException(error.getCode(),  error.getDetail());
            //throw new RestApplicationException(HttpStatus.BAD_REQUEST, ApiError.ILLEGAL_ARGUMENT_TYPE, error.getDetail());
            
        }
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }

    /**
     * Default application error request mapping: Returns always 503 service unavailable
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = Endpoints.SPRING_BOOT_ERRORS, method={RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT,RequestMethod.HEAD})
    public ResponseEntity getError(HttpServletRequest request, HttpServletResponse response)
    {       
        Map<String, Object> attributes = getErrorAttributes(request, false);
        String title = (String) request.getAttribute("title");
        String details = (String) request.getAttribute("details");        
        String status = String.valueOf(attributes.get("status"));
        String message = String.valueOf(attributes.get("message"));       
        
        switch (status) {

            case "403":                                
                details = message;
                message = HttpStatus.FORBIDDEN.getReasonPhrase();
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");                
                break;

            case "404":                
                details = message;
                message = HttpStatus.NOT_FOUND.getReasonPhrase();
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");                             
                throw new ResourceNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, null);                

            case "405":                
                details = message;
                message = HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase();
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");                
                break;

            case "500":
                details = message;
                message = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");                
                break;

            default:
                details = message;                
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");                
                break;

        }       
        
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }    
    
    /**
     * Return the errors for the request.
     *
     * @param request The HTTP Request processed
     * @param includeStackTrace
     * @return	Map
     */
    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);      
        ServletWebRequest webRequest = new ServletWebRequest(request);
        return errorAttributes.getErrorAttributes(webRequest, includeStackTrace);
        //throw new NotImplementedException("error attributes not defined yet.");
        
    }

    /**
     * Return the Error Endpoint
     *
     * @return String	Endpoint Path
     */
    @Override
    public String getErrorPath() {
        return Endpoints.SPRING_BOOT_ERRORS;
    }

}
