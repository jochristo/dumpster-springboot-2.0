package joch.armada.api.handler;

import java.util.List;
import org.springframework.validation.FieldError;

/**
 *
 * @author ic
 */
public interface IFieldValidationHandler {
 
    public void resolveErrors(List<FieldError> fieldErrors);
    
    public void resolveErrors(FieldError fieldError);
    
    public void resolveErrors(List<FieldError> fieldErrors, Object target);
}
