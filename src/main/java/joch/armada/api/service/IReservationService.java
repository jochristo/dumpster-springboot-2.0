package joch.armada.api.service;

import java.util.Date;
import java.util.List;
import joch.armada.api.domain.Reservation;
import joch.armada.data.mongo.service.IMongoDbService;

/**
 *
 * @author Admin
 */
public interface IReservationService extends IMongoDbService<Reservation>
{             
    public List<Reservation> findByDate(Date createdAt);         
    
    public boolean exists(String id);        
    
    public boolean existsCustomerId(String customerId);              
    
    public boolean existsPlacementId(String placementId);              
    
    public List<Reservation> findUndeleted();              
    
    public List<Reservation> findDeleted();        
    
}
