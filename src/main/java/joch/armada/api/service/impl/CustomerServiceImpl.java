package joch.armada.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.api.exception.ResourceIdentifierNotFoundException;
import joch.armada.api.domain.Customer;
import joch.armada.data.mongo.predicate.EntityPredicateBuilder;
import joch.armada.data.mongo.predicate.PredicateOperation;
import joch.armada.api.repository.ICustomerRepository;
import joch.armada.api.service.ICustomerService;
import org.joko.core.utilities.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ic
 */
@Service
public class CustomerServiceImpl implements ICustomerService //IMongoDbService<Customer>
{
    @Autowired
    private ICustomerRepository repository;

    @Override
    public List<Customer> get() {
        return repository.findUnDeleted();
    }

    @Override
    public Customer findOne(String id) {
        return repository.findOne(id);
    }

    @Override
    public Customer save(Customer object) {
        return repository.save(object);
    }

    @Override
    public Customer update(Object id, Customer object)
    {
        Customer customer = this.findOne("id", id);
        if(customer == null) throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Customer id not found: " + id);                 
        return repository.save(customer.updateInstance(object));
    }

    @Override
    public void delete(Customer object) {
        repository.delete(object);
    }

    @Override
    public void insert(Customer object)
    {               
        repository.insert(object);
    }

    @Override
    public Customer findOne(Object object) {        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Customer findOne(String key, Object value)
    {                
        EntityPredicateBuilder<Customer> predicate = new EntityPredicateBuilder(Customer.class);
        predicate.with(key, PredicateOperation.EQUALS, value);
        //return repository.findOne(predicate.build());        
        return repository.findOne(predicate.build()).get();        
    }    

    @Override
    public List<Customer> get(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<Customer> find(Map<Object,Object> requestMap)
    {
        if(Utilities.isEmpty(requestMap) == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Customer> predicate = new EntityPredicateBuilder(Customer.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){                    
                    predicate.with((String) key, PredicateOperation.EQUALS, value);
                }                                
            }));     
            List<Customer> list = new ArrayList();
            Iterable<Customer> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null;           
    }
    
    @Override
    public List<Customer> find(Map<Object,Object> requestMap, PredicateOperation operation)
    {
        if(Utilities.isEmpty(requestMap) == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Customer> predicate = new EntityPredicateBuilder(Customer.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){
                    //predicate.with((String) key, ":", value);
                    Object[] readValue = EntityPredicateBuilder.determineValue(key, value);
                    //value = readValue;
                    predicate.with((String) key, operation, value, readValue);
                }                                
            }));     
            List<Customer> list = new ArrayList();
            Iterable<Customer> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null;           
    }           

    @Override
    public Customer findByEmail(String email) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer findByMobileNo(String mobileNo) {
        return repository.findByMobileNo(mobileNo);
    }

    @Override
    public Customer findByPhoneNo(String phoneNo) {
        return repository.findByPhoneNo(phoneNo);
    }

    @Override
    public List<Customer> findByLastname(String lastname) {
        return repository.findByLastname(lastname);
    }

    @Override
    public List<Customer> findUnDeleted() {
        return repository.findUnDeleted();
    }

    @Override
    public List<Customer> findDeleted() {
        return repository.findDeleted();
    }
    
}
