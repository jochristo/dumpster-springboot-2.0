package joch.armada.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import joch.armada.api.domain.Placement;
import joch.armada.api.domain.Reservation;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.data.mongo.predicate.EntityPredicateBuilder;
import joch.armada.data.mongo.predicate.PredicateOperation;
import joch.armada.api.repository.IReservationRepository;
import joch.armada.api.service.IReservationService;
import joch.armada.core.exception.DataConstraintViolationException;
import org.joko.core.utilities.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ic
 */
@Service("reservationService")
public class ReservationServiceImpl implements IReservationService
{
    @Autowired
    private IReservationRepository repository;     

    @Override
    public List<Reservation> get() {
        return repository.findUnDeleted();
    }

    @Override
    public Reservation findOne(String id) {
        return repository.findOne(id);
    }

    @Override
    public Reservation save(Reservation object) {
        return repository.save(object);
    }

    @Override
    public void delete(Reservation object) {
        repository.delete(object);
    }

    @Override
    public void insert(Reservation object) {
        repository.insert(object);
    }

    @Override
    public Reservation findOne(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Reservation> get(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Reservation update(Object id, Reservation object) {
        Reservation reservation = repository.findOne((String)id);
        if(reservation != null)
        {
            reservation.setCustomer(object.getCustomer());
            reservation.setPlacements(object.getPlacements());
            reservation.setDeleted(object.isDeleted());
        }
        return repository.save(reservation);    
    }

    @Override
    public List<Reservation> find(Map<Object, Object> requestMap)
    {
        if(Utilities.isEmpty(requestMap) == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Reservation> predicate = new EntityPredicateBuilder(Reservation.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){
                    predicate.with((String) key, ":", value);
                }                                
            }));     
            List<Reservation> list = new ArrayList();
            Iterable<Reservation> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null;      
    }

    @Override
    public List<Reservation> find(Map<Object, Object> requestMap, PredicateOperation operation) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Reservation findOne(String key, Object value) {
        EntityPredicateBuilder<Reservation> predicate = new EntityPredicateBuilder(Reservation.class);
        predicate.with(key, PredicateOperation.EQUALS, value);
        //return repository.findOne(predicate.build());  
        return repository.findOne(predicate.build()).get();  
    }

    @Override
    public List<Reservation> findByDate(Date createdAt) {
        return repository.findByDate(createdAt);
    }

    @Override
    public boolean exists(String id) {
        return repository.exists(id);
    }

    @Override
    public boolean existsCustomerId(String customerId) {
        return repository.existsCustomerId(customerId);
    }

    @Override
    public List<Reservation> findUndeleted() {
        return repository.findUnDeleted();
    }

    @Override
    public List<Reservation> findDeleted() {
        return repository.findDeleted();
    }

    @Override
    public boolean existsPlacementId(String placementId) {
        List<Reservation> list = this.findUndeleted();
        Predicate<Placement> predicate = p -> p.getId().equals(placementId); 
        
        if(!list.isEmpty())
        {
            list.stream().forEach((res)->{
                List<Placement> placements = res.getPlacements();
                if(placements != null)
                {
                    if(placements.stream().anyMatch(predicate)){
                        throw new DataConstraintViolationException(ApiErrorCode.DATA_INTEGRITY_VIOLATION, "Cannon delete reservation because it has a placement foreign key dependency: " + placementId);
                        //return true;
                        
                    }
                }
            });                            
        }
        return false;
    }

    //@Override
    @Deprecated
    public boolean hasPlacementDependency() {
        List<Reservation> list = this.findUndeleted();
        if (!list.isEmpty()) {
            list.stream().forEach((res) -> {
                List<Placement> placements = res.getPlacements();
                if (placements != null) {
                    throw new DataConstraintViolationException(ApiErrorCode.DATA_INTEGRITY_VIOLATION, "Cannon delete reservation because it has a placement foreign key dependencies");                    
                }

            });
        }
        return false;
    }
    
    

    
    
}
