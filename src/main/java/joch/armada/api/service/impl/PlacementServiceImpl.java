package joch.armada.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import joch.armada.api.domain.Placement;
import joch.armada.api.domain.Reservation;
import joch.armada.api.domain.enumeration.PlacementStatus;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.api.model.bind.PlacementQueryParameter;
import joch.armada.data.mongo.predicate.EntityPredicateBuilder;
import joch.armada.data.mongo.predicate.PredicateOperation;
import joch.armada.api.repository.IPlacementRepository;
import joch.armada.api.service.IPlacementService;
import joch.armada.api.service.IReservationService;
import joch.armada.core.exception.DataConstraintViolationException;
import org.joko.core.utilities.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ic
 */
@Service("placementService")
public class PlacementServiceImpl implements IPlacementService
{
    @Autowired private IPlacementRepository repository;            
    @Autowired private IReservationService reservationService;    

    @Override
    public List<Placement> get() {
        return repository.findUndeleted();
    }

    @Override
    public Placement findOne(String id) {
        return repository.findOne(id);
    }

    @Override
    public Placement save(Placement placement) {
        return repository.save(placement);
    }

    @Override
    public void delete(Placement placement) {
        repository.delete(placement);
    }

    @Override
    public void insert(Placement placement) {
        repository.insert(placement);
    }

    @Override
    public Placement findOne(Object object) {
        Placement placement = new Placement();        
        return null;
    }

    @Override
    public List<Placement> get(Object object) {
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    

    @Override
    public Placement update(Object id, Placement object) {
        Placement placement = repository.findOne((String)id);
        if(placement != null)
        {            
            placement.setAddress(object.getAddress());
            placement.setDateIn(object.getDateIn());
            placement.setDateOut(object.getDateOut());
            placement.setLatitude(object.getLatitude());
            placement.setLongitude(object.getLongitude());
            placement.setStatus(object.getStatus());
            placement.setDeleted(object.isDeleted());
        }
        
        // check for navigation property dependency, i.e. foreign key dependency
        if(placement.isDeleted() == true){
            
            hasReservationDependency(placement.getId());
        }
        
        return repository.save(placement);
    }

    @Override
    public List<Placement> find(Map<Object, Object> requestMap)
    {
        if(Utilities.isEmpty(requestMap) == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Placement> predicate = new EntityPredicateBuilder(Placement.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){
                    predicate.with((String) key, ":", value);
                }                                
            }));     
            List<Placement> list = new ArrayList();
            Iterable<Placement> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null;               
    }

    @Override
    public List<Placement> find(Map<Object, Object> requestMap, PredicateOperation operation) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Placement findOne(String key, Object value) {
        EntityPredicateBuilder<Placement> predicate = new EntityPredicateBuilder(Placement.class);
        predicate.with(key, PredicateOperation.EQUALS, value);
        //return repository.findOne(predicate.build());  
        return repository.findOne(predicate.build()).get();  
    }

    @Override
    public List<Placement> find(PlacementStatus status) {
        return repository.find(status);
    }

    @Override
    public List<Placement> find(double lonMin, double lonMax, double latMin, double latMax) {
        return repository.find(lonMin, lonMax, latMin, latMax);
    }

    @Override
    public List<Placement> findByDateIn(Date dateInFrom, Date dateInTo) {
        return repository.findByDateIn(dateInFrom, dateInTo);
    }

    @Override
    public List<Placement> findByDateOut(Date dateOut) {
        return repository.findByDateOut(dateOut);
    }

    @Override
    public boolean exists(String id) {
        return repository.exists(id);
    }

    @Override
    public boolean existsDumpsterReferenceId(String dumpsterReferenceId) {
        return repository.existsDumpsterReferenceId(dumpsterReferenceId);
    }

    @Override
    public List<Placement> findUndeleted() {
        return repository.findUndeleted();
    }

    @Override
    public List<Placement> findDeleted() {
        return repository.findDeleted();
    }

    @Override
    public List<Placement> find(PlacementQueryParameter params) {
        // create criteria query...
        // to do
        return repository.findUndeleted();
    }
    
    @Override
    public boolean hasReservationDependency(String placementId) {
        List<Reservation> list = reservationService.findUndeleted();
        Predicate<Placement> predicate = p -> p.getId().equals(placementId); 
        
        if(!list.isEmpty())
        {
            list.stream().forEach((res)->{
                List<Placement> placements = res.getPlacements();
                if(placements != null)
                {
                    if(placements.stream().anyMatch(predicate)){
                        throw new DataConstraintViolationException(ApiErrorCode.DATA_INTEGRITY_VIOLATION, 
                            "Cannon delete placement id <" + placementId + "> because it has a reservation document reference dependency: " + res.getId());
                        //return true;
                        
                    }
                }
            });                            
        }
        return false;
    }    
    
    
    
}
