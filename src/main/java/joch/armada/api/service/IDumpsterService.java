package joch.armada.api.service;

import joch.armada.api.domain.Dumpster;
import joch.armada.data.mongo.service.IMongoDbService;

/**
 *
 * @author Admin
 */
public interface IDumpsterService extends IMongoDbService<Dumpster>
{
    
}
