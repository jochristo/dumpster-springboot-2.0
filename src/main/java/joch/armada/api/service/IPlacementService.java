package joch.armada.api.service;

import java.util.Date;
import java.util.List;
import joch.armada.api.domain.Placement;
import joch.armada.api.domain.enumeration.PlacementStatus;
import joch.armada.api.model.bind.PlacementQueryParameter;
import joch.armada.data.mongo.service.IMongoDbService;

/**
 *
 * @author Admin
 */
public interface IPlacementService extends IMongoDbService<Placement>
{        
    public List<Placement> find(PlacementStatus status); 
        
    public List<Placement> find(double lonMin, double lonMax, double latMin, double latMax);    
        
    public List<Placement> findByDateIn(Date dateInFrom, Date dateInTo);     
        
    public List<Placement> findByDateOut(Date dateOut);
    
    public boolean exists(String id);       

    public boolean existsDumpsterReferenceId(String dumpsterReferenceId);              
    
    public List<Placement> findUndeleted();              
    
    public List<Placement> findDeleted();      
    
    public List<Placement> find(PlacementQueryParameter params);
    
    public boolean hasReservationDependency(String placementId);
}
