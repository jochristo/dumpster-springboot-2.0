package joch.armada.api.service;

import java.util.List;
import joch.armada.api.domain.Customer;
import joch.armada.data.mongo.service.IMongoDbService;

/**
 *
 * @author Admin
 */
public interface ICustomerService extends IMongoDbService<Customer>
{    
    public Customer findByEmail(String email);    
    
    public Customer findByMobileNo(String mobileNo);    
    
    public Customer findByPhoneNo(String phoneNo);    
    
    public List<Customer> findByLastname(String lastname);            
    
    public List<Customer> findUnDeleted();     
    
    public List<Customer> findDeleted();     
}
