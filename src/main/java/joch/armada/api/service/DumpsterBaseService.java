package joch.armada.api.service;

import java.util.List;
import java.util.Map;
import joch.armada.api.domain.Dumpster;
import joch.armada.data.mongo.predicate.BaseQueryPredicateService;
import joch.armada.api.repository.IBaseMongoRepository;
import org.springframework.stereotype.Component;

/**
 *
 * @author Admin
 */
@Component
public class DumpsterBaseService extends BaseQueryPredicateService<Dumpster>
{       

    
    @Override
    public List<Dumpster> findWithMap(Map<Object, Object> requestMap) {
        return super.findWithMap(requestMap); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Dumpster> findWithMap(IBaseMongoRepository<Dumpster> repository, Map<Object, Object> requestMap) {
        return super.findWithMap(repository, requestMap); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
