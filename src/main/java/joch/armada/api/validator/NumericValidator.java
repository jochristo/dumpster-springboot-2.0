package joch.armada.api.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import joch.armada.api.annotation.NumericAttribute;
import static joch.armada.api.annotation.NumericAttribute.AttributeType.LATITUDE;
import static joch.armada.api.annotation.NumericAttribute.AttributeType.LONGITUDE;
import org.joko.core.utilities.Utilities;

/**
 * Constraint validity class for profile properties: latitude, longitude, mobile no, and generic numbers.
 * @author ic
 */
public class NumericValidator implements ConstraintValidator<NumericAttribute, Object>
{
    //regular expression matching patterns
    private final String LatitudePattern = "^-?(([1-8]?[0-9]\\.{1}\\d{1,})|([1-8]?[0-9])$|(90\\.{1}0{1,})|(90)$)"; //"^-?([1-8]?[0-9]\\.{1}\\d{1,}$|90\\.{1}0{1,}$)";        
    private final String LongitudePattern = "^-?((([1]?[0-7][0-9]|[1-9]?[0-9])(\\.{1}\\d{1,})?$)|[1]?[1-8][0](\\.{1}0{1,})?$)"; //"^-?((([1]?[0-7][0-9]|[1-9]?[0-9])\\.{1}\\d{1,}$)|[1]?[1-8][0]\\.{1}0{1,}$)";       
    private final String generic = "^[0]|\\d+$";
    private final String mobileNoPattern = "^[+][0-9]+\\d+$";    
    private NumericAttribute numericAttribute;   
   
    //pattern to use
    private String pattern = null;        
        
    @Override
    public void initialize(NumericAttribute a) {
        
        NumericAttribute.AttributeType attributeType = a.pattern();
        numericAttribute = a;
        switch (attributeType)
        {
            case LONGITUDE: 
                pattern = LongitudePattern;
                break;            
            case LATITUDE: 
                this.pattern = LatitudePattern;
                break;
            case GENERAL_NUMBER: 
                pattern = generic;
                break;
            case MOBILE_NUMBER:
                pattern = mobileNoPattern;
                break;                
        }   
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext cvc) 
    {           
        if(Utilities.isEmpty(object))
        {
            return true;
        }
        
        // special case for long/lat: 0 or 0.0
        /*
        if(numericAttribute.pattern() == LONGITUDE || numericAttribute.pattern() == LATITUDE){
            if(String.valueOf(object).equals("0.0")){
                return false;
            }         
        }
        */
        return String.valueOf(object).matches(pattern);
    }
        
}