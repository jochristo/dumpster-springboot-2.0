package joch.armada.api.validator;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import joch.armada.api.annotation.DateAttribute;

/**
 *
 * @author ic
 */
public class DateFormatValidator implements ConstraintValidator<DateAttribute,String>
{
    
    private final String shortDashPattern = "dd-MM-yyyy";
    private final String shortSlashPattern = "dd/MM/yyyy";    
    private final String matcherWithDash = "^(3[01]|[12][0-9]|0[1-9])\\-(1[0-2]|0[1-9])\\-[0-9]{4}$";
    private final String matcherWithSlash = "^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$";    
    private String pattern;

    @Override
    public void initialize(DateAttribute a) {        
       
    }

    @Override
    public boolean isValid(String object, ConstraintValidatorContext cvc)
    {
        
        if ( object == null ) {
            return true;
        }        
        if(object.equals("")){
            return true;
        } 
        try{
            LocalDate ld = LocalDate.parse(object, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            //ld = LocalDate.parse(object, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            //String temp = ld.toString();
        }
        catch (Exception ex){
            //logger.error("IllegalArgumentException occured during date conversion... "+ ex.getMessage());   
            //throw new InvalidArgumentValueException(ApiErrorCode.INVALID_PARAMETER_VALUE, "Given date format is invalid, use: 'dd-mm-yyyy'");
            return false;
        }        
        //this.pattern = (object.matches(matcherWithDash)) ? shortDashPattern : shortSlashPattern;                
        //boolean isMatch = object.matches(pattern);
        //return isMatch;
        return true;
    }

    
}
