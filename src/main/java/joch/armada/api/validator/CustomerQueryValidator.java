package joch.armada.api.validator;

import joch.armada.api.model.CustomerQuery;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author ic
 */
public class CustomerQueryValidator implements Validator
{

    @Override
    public boolean supports(Class<?> type) {
        return CustomerQueryValidator.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors)
    {
        CustomerQuery cq = (CustomerQuery)o;        
        
    }
    
}
