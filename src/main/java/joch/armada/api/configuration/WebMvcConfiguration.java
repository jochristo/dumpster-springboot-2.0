package joch.armada.api.configuration;

import joch.armada.api.interceptor.HttpRequestInterceptor;
import joch.armada.api.interceptor.LoggerInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 * @author ic
 */
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer
{    
    @Autowired private HttpRequestInterceptor httpRequestInterceptor;    
    @Autowired private LoggerInterceptor loggerInterceptor;
    

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(httpRequestInterceptor); //To change body of generated methods, choose Tools | Templates.        
    }
    
    
}
