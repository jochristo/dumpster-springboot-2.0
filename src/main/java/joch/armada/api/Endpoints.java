package joch.armada.api;

/**
 *
 * @author ic
 */
public interface Endpoints
{    
    public static final String APP_CONTEXT_PATH = "";
    
    public static final String API_PATH = APP_CONTEXT_PATH + "/api/v1";
    
    public static final String SPRING_BOOT_ERRORS = "/error";	          
    
    public static final String APPLICATION_CODES = "/appcodes";	    

    public static final String AUTHENTICATION_LOGIN = API_PATH + "/auth/login";

    public static final String AUTHENTICATION_LOGOUT = API_PATH + "/auth/logout";
    
    public static final String AUTHENTICATION_SIGNUP = API_PATH + "/auth/signup";            
    
    public static final String PLACEMENTS = API_PATH + "/placements";         
    
    public static final String PLACEMENT_BY_ID = API_PATH + "/placements/{id}";      
    
    public static final String RESERVATIONS = API_PATH + "/reservations"; 
    
    public static final String RESERVATION_BY_ID = API_PATH + "/reservations/{id}";      
    
    public static final String TRANSACTIONS = API_PATH + "/transactions";      
    
    public static final String DUMPSTERS = API_PATH + "/dumpsters";        
    
    public static final String DUMPSTERS_BY_ID = API_PATH + "/dumpsters/{id}";     
    
    public static final String CUSTOMERS = API_PATH + "/customers";         
    
    public static final String CUSTOMERS_BY_ID = API_PATH + "/customers/{id}";         
        
}
