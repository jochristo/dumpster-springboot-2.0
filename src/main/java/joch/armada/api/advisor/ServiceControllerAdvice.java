package joch.armada.api.advisor;

import java.util.ArrayList;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import joch.armada.api.error.ApiError;
import joch.armada.core.exception.InvalidArgumentValueException;
import org.joko.core.jsonapi.JSONApiDocument;
import org.joko.core.jsonapi.JSONApiErrors;
import org.joko.core.jsonapi.helpers.ErrorSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Service annotated component advisor class.
 *
 * @author ic
 */
@ControllerAdvice(annotations = {Service.class})
public class ServiceControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(ServiceControllerAdvice.class);

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseBody
    public JSONApiDocument handleConstraintViolation(ConstraintViolationException ex) {
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
        JSONApiErrors error = null;

        Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
        for (ConstraintViolation<?> violation : violations) {
            error = new JSONApiErrors();
            error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
            error.setSource(new ErrorSource(violation.getPropertyPath().toString()));
            error.setTitle("Invalid " + violation.getPropertyPath().toString());
            error.setDetail(violation.getMessage().substring(0, 1).toUpperCase() + violation.getMessage().substring(1));
            errorsList.add(error);
        }
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    public JSONApiDocument handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
        error.setCode(ApiError.CONSTRAINT_VIOLATION.getAppCode());
        error.setTitle(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());
        error.setDetail(ApiError.CONSTRAINT_VIOLATION.getDetails());
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({DataIntegrityViolationException.class})
    @ResponseBody
    public JSONApiDocument handleDataIntegrityViolation(DataIntegrityViolationException ex) {

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
        error.setCode(ApiError.CONSTRAINT_VIOLATION.getAppCode());
        error.setTitle(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());        
        error.setDetail(ex.getMostSpecificCause().getMessage());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }

    /**
     * AbstractCoreApplicationException-derived classes handler
     *
     * @param ex
     * @return
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({InvalidArgumentValueException.class})
    @ResponseBody
    public JSONApiDocument handleInvalidArgumentValueException(InvalidArgumentValueException ex) {

        logger.error("handleInvalidArgumentValueException occured: " + ex.getClass().getSimpleName(), ex);

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.BAD_REQUEST.toString());
        error.setCode(ApiError.BAD_REQUEST.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());
        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }
}
