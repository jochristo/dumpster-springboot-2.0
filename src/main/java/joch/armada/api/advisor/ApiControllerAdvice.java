package joch.armada.api.advisor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.joko.core.jsonapi.JSONApiDocument;
import org.joko.core.jsonapi.JSONApiErrors;
import org.joko.core.jsonapi.helpers.ErrorSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.metadata.ConstraintDescriptor;
import joch.armada.api.annotation.NumericAttribute;
import joch.armada.api.error.ApiError;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.api.error.ErrorResponse;
import joch.armada.api.exception.FieldValidationException;
import joch.armada.api.exception.InvalidParameterArgumentException;
import joch.armada.api.exception.MissingRequestParameterException;
import joch.armada.api.exception.ParameterConstraintViolationException;
import joch.armada.api.exception.ResourceIdentifierNotFoundException;
import joch.armada.api.exception.ResourceNotFoundException;
import joch.armada.core.exception.ApplicationConstraintException;
import joch.armada.core.exception.DataConstraintViolationException;
import joch.armada.core.exception.GeneralApplicationCoreException;
import joch.armada.core.exception.InvalidArgumentTypeException;
import joch.armada.core.exception.InvalidArgumentValueException;

/**
 *
 * @author ic
 */
@ControllerAdvice(annotations = {RestController.class})
public class ApiControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(ApiControllerAdvice.class);     
     
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({FieldValidationException.class})
    @ResponseBody    
    public JSONApiDocument handleFieldValidationException(FieldValidationException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        
        List<ErrorResponse> errors = ex.getErrorResponses();
        if(errors != null){
            errors.stream().forEachOrdered((item)->{
                JSONApiErrors error = new JSONApiErrors();
                error.setStatus(ex.getApiErrorCode().getStatusAsString());
                error.setCode(ex.getApiErrorCode().getAppCode());
                error.setTitle(ex.getApiErrorCode().getTitle());
                error.setDetail(item.getError());        
                errorsList.add(error);                
            });
        }     
        else{
            JSONApiErrors error = new JSONApiErrors();
            error.setStatus(ex.getApiErrorCode().getStatusAsString());
            error.setCode(ex.getApiErrorCode().getAppCode());
            error.setTitle(ex.getApiErrorCode().getTitle());
            error.setDetail(ex.getErrorResponse().getError());
            errorsList.add(error);         
        }
        
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }     
    
    /**
     * ParameterConstraintViolationException handler
     * @param ex
     * @return 
     */    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ParameterConstraintViolationException.class})
    @ResponseBody
    public JSONApiDocument handleParameterConstraintViolationException(ParameterConstraintViolationException ex) {

        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);   

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = null;

        Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
        for (ConstraintViolation<?> violation : violations) {
            error = new JSONApiErrors();            
            error.setCode(ApiErrorCode.CONSTRAINT_VIOLATION.getAppCode());
            error.setStatus(HttpStatus.BAD_REQUEST.toString());
            error.setSource(new ErrorSource(violation.getPropertyPath().toString()));            
            error.setTitle(ApiErrorCode.CONSTRAINT_VIOLATION.getTitle());            
            
            // read custom constraint annotation attributes - set code
            ConstraintDescriptor descriptor = violation.getConstraintDescriptor();
            if(descriptor.getAnnotation() instanceof NumericAttribute){
                NumericAttribute numericAttribute = (NumericAttribute) descriptor.getAnnotation();
                error.setCode(numericAttribute.errorCode().getCodeAsString());
            }
            
            error.setDetail(violation.getMessage().substring(0, 1).toUpperCase() + violation.getMessage().substring(1));
            errorsList.add(error);
            logger.info("Parameter constraint violation occured: " + violation.getMessage(), ex);
        }
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }    
    
    /**
     * MissingRequestParameterException handler
     * @param ex
     * @return 
     */    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MissingRequestParameterException.class})
    @ResponseBody
    public JSONApiDocument handleMissingRequestParameterException(MissingRequestParameterException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);   
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ex.getHttpStatus().toString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }    
    
    /**
     * InvalidParameterArgumentException handler
     * @param ex
     * @return 
     */    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({InvalidParameterArgumentException.class})
    @ResponseBody
    public JSONApiDocument handleInvalidParameterArgumentException(InvalidParameterArgumentException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);   
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ex.getHttpStatus().toString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }    
    
    /**
     * ResourceIdentifierNotFoundException handler
     * @param ex
     * @return 
     */    
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler({ResourceIdentifierNotFoundException.class})
    @ResponseBody
    public JSONApiDocument handleResourceIdentifierNotFoundException(ResourceIdentifierNotFoundException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);   
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ex.getHttpStatus().toString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }        
    
    /**
     * ResourceNotFoundException handler
     * @param ex
     * @return 
     */    
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler({ResourceNotFoundException.class})
    @ResponseBody
    public JSONApiDocument handleResourceNotFoundException(ResourceNotFoundException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);   
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ApiErrorCode.RESOURCE_NOT_FOUND.getStatusAsString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }     
    
    /**
     * InvalidArgumentTypeException handler
     * @param ex
     * @return 
     */    
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({InvalidArgumentTypeException.class})
    @ResponseBody
    public JSONApiDocument handleInvalidArgumentTypeException(InvalidArgumentTypeException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);   
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
        error.setCode(ex.getCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }     
    
    /**
     * InvalidArgumentValueException classes handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({InvalidArgumentValueException.class})
    @ResponseBody
    public JSONApiDocument handleInvalidArgumentValueException(InvalidArgumentValueException ex) {
        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);   

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
        error.setCode(ex.getCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());
        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }         
  
    /**
     * DataConstraintViolationException classes handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({DataConstraintViolationException.class})
    @ResponseBody
    public JSONApiDocument handleDataConstraintViolationException(DataConstraintViolationException ex) {
        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);   

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
        error.setCode(ex.getCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());
        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }     
    
    /**
     * ApplicationConstraintException classes handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({ApplicationConstraintException.class})
    @ResponseBody
    public JSONApiDocument handleApplicationConstraintException(ApplicationConstraintException ex) {
        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);   

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
        error.setCode(ex.getCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());
        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }         
    
    /**
     * GeneralApplicationCoreException classes handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({GeneralApplicationCoreException.class})
    @ResponseBody
    public JSONApiDocument handleApplicationConstraintException(GeneralApplicationCoreException ex) {
        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);   

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        error.setCode(ex.getCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());
        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }         
    
    
    /**
     * TransactionSystemException handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    //@ExceptionHandler({org.springframework.transaction.TransactionSystemException.class})
    @ResponseBody
    public JSONApiDocument handleTransactionSystem(TransactionSystemException ex) {

        logger.error("TransactionSystemException occured: ", ex.getMostSpecificCause());

        JSONApiDocument errorDocument = new JSONApiDocument();

        if (ex.getMostSpecificCause() instanceof ConstraintViolationException) {
            ConstraintViolationException constrainViolation = (ConstraintViolationException) ex.getMostSpecificCause();
            ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
            JSONApiErrors error = null;

            Set<ConstraintViolation<?>> violations = constrainViolation.getConstraintViolations();
            for (ConstraintViolation<?> violation : violations) {
                error = new JSONApiErrors();
                error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
                error.setSource(new ErrorSource(violation.getPropertyPath().toString()));
                error.setTitle("Invalid " + violation.getPropertyPath().toString());
                error.setDetail(violation.getMessage().substring(0, 1).toUpperCase() + violation.getMessage().substring(1));
                errorsList.add(error);
            }
            errorDocument.setErrors(errorsList);
        } else {
            System.out.println("Unimplement Transaction System Error");
        }

        return errorDocument;

    }

    /**
     * ConstraintViolationException handler
     * @param ex
     * @return 
     */    
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    //@ExceptionHandler({ConstraintViolationException.class})
    @ResponseBody
    public JSONApiDocument handleConstraintViolationException(ConstraintViolationException ex) {

        logger.error("ConstraintViolationException occured: ", ex);

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = null;

        Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
        for (ConstraintViolation<?> violation : violations) {
            error = new JSONApiErrors();            
            error.setCode(ApiError.CONSTRAINT_VIOLATION.getAppCode());
            error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
            error.setSource(new ErrorSource(violation.getPropertyPath().toString()));            
            error.setTitle(ApiError.CONSTRAINT_VIOLATION.getDetails());          
            
            // read custom constraint annotation attributes - set code
            ConstraintDescriptor descriptor = violation.getConstraintDescriptor();
            if(descriptor.getAnnotation() instanceof NumericAttribute){
                NumericAttribute numericAttribute = (NumericAttribute) descriptor.getAnnotation();
                error.setCode(numericAttribute.errorCode().getCodeAsString());
            }
            
            error.setDetail(violation.getMessage().substring(0, 1).toUpperCase() + violation.getMessage().substring(1));
            errorsList.add(error);
            logger.info("Violation occured: " + violation.getMessage(), ex);
        }
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }
               
    /**
     * MethodArgumentNotValidException handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    //@ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    public JSONApiDocument handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {

        logger.error("MethodArgumentNotValidException occured: ", ex);

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
        error.setCode(ApiError.CONSTRAINT_VIOLATION.getAppCode());
        error.setTitle(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());
        error.setDetail(ApiError.CONSTRAINT_VIOLATION.getDetails());
        
        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }

    /**
     * DataIntegrityViolationException handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    //@ExceptionHandler({DataIntegrityViolationException.class})
    @ResponseBody
    public JSONApiDocument handleDataIntegrityViolation(DataIntegrityViolationException ex) {

        logger.error("DataIntegrityViolationException occured: ", ex.getMostSpecificCause());

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
        error.setCode(ApiError.CONSTRAINT_VIOLATION.getAppCode());
        error.setTitle(ApiError.DATA_VIOLATION_EXCEPTION.getDetails());        
        error.setDetail(ex.getMostSpecificCause().getMessage());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }

    /**
     * HttpMessageNotReadableException, MethodArgumentTypeMismatchException, NumberFormatException, IllegalArgumentException, NullPointerException
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    //@ExceptionHandler({HttpMessageNotReadableException.class, MethodArgumentTypeMismatchException.class, NumberFormatException.class, IllegalArgumentException.class, NullPointerException.class})
    @ResponseBody
    public JSONApiDocument handleMethodArgumentTypeMismatch(Exception ex) {

        logger.error("handleMethodArgumentTypeMismatch occured: " + ex.getClass().getSimpleName() , ex);

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.BAD_REQUEST.toString());
        error.setCode(ApiError.ILLEGAL_ARGUMENT_TYPE.getAppCode());
        error.setTitle(HttpStatus.BAD_REQUEST.getReasonPhrase());
        error.setDetail(ApiError.ILLEGAL_ARGUMENT_TYPE.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }

    /**
     * HttpRequestMethodNotSupportedException handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    //@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    public JSONApiDocument handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex) {

        logger.error("handleHttpRequestMethodNotSupported occured: ", ex);

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.BAD_REQUEST.toString());
        error.setCode(ApiError.ILLEGAL_ARGUMENT_TYPE.getAppCode());
        error.setTitle(HttpStatus.BAD_REQUEST.getReasonPhrase());
        error.setDetail(ApiError.ILLEGAL_ARGUMENT_TYPE.getDetails());
        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }

    /**
     * IOException, NestedServletException, ConversionNotSupportedException, HttpMessageNotWritableException handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    //@ExceptionHandler({IOException.class, NestedServletException.class, ConversionNotSupportedException.class, HttpMessageNotWritableException.class})
    @ResponseBody
    public JSONApiDocument handleIOException(Exception ex) {

        logger.error("handleIOException occured: " + ex.getClass().getSimpleName(), ex);

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        error.setCode(ApiError.INTERNAL_SERVER_ERROR.getAppCode());
        error.setTitle(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        error.setDetail(ApiError.INTERNAL_SERVER_ERROR.getDetails());
        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }

    /**
     * HttpMediaTypeNotSupportedException handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    //@ExceptionHandler({HttpMediaTypeNotSupportedException.class})
    @ResponseBody
    public JSONApiDocument handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException ex) {

        logger.error("handleHttpMediaTypeNotSupportedException occured: " , ex);

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE.toString());
        error.setCode(ApiError.UNSUPPORTED_MEDIA_TYPE.getAppCode());
        error.setTitle(HttpStatus.UNSUPPORTED_MEDIA_TYPE.getReasonPhrase());
        error.setDetail(ApiError.UNSUPPORTED_MEDIA_TYPE.getDetails());
        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }

    /**
     * MissingServletRequestParameterException, ServletRequestBindingException, MissingServletRequestPartException, TypeMismatchException handler
     * @param ex
     * @return 
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    //@ExceptionHandler({MissingServletRequestParameterException.class, ServletRequestBindingException.class, MissingServletRequestPartException.class, TypeMismatchException.class})
    @ResponseBody
    public JSONApiDocument handleGeneralBadRequests(Exception ex) {

        logger.error("handleGeneralBadRequests occured: " + ex.getClass().getSimpleName() , ex);

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.BAD_REQUEST.toString());
        error.setCode(ApiError.ILLEGAL_ARGUMENT_TYPE.getAppCode());
        error.setTitle(HttpStatus.BAD_REQUEST.getReasonPhrase());
        error.setDetail(ApiError.ILLEGAL_ARGUMENT_TYPE.getDetails());
        
        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }     
    

    
  
         
     /**
     * General Throwable class handler
     * @param ex
     * @return 
     */
    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JSONApiDocument handleThrowable(Throwable ex)
    {
        logger.error("Throwable occured: ", ex);
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.BAD_REQUEST.toString());
        error.setCode("400");
        error.setTitle("Mastercard API exception");        
        error.setDetail(ex.getMessage());
        error.setSource(new ErrorSource("System"));

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;

        /*
        logger.error(ex.toString(), ex);

        Error error = new Error();
        error.setSource("System");
        error.setReasonCode(ex.getMessage());

        Errors errors = new Errors();
        errors.addErrorItem(error);

        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        */
    }    
    
}
