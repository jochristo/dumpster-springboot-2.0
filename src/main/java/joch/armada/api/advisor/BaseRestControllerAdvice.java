package joch.armada.api.advisor;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import joch.armada.api.error.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.util.NestedServletException;

/**
 *
 * @author ic
 */
@ControllerAdvice(annotations = {RestController.class})
public class BaseRestControllerAdvice
{
    private static final Logger logger = LoggerFactory.getLogger(BaseRestControllerAdvice.class);          
    
    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map handle(MethodArgumentNotValidException exception) {
        String type  = exception.getClass().getSimpleName();        
        logger.error(type + " occured: ", exception);           
        return error(exception.getBindingResult().getFieldErrors()
                .stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.toList()));
    }

    @ExceptionHandler({ConstraintViolationException .class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map handle(ConstraintViolationException exception) {
        String type  = exception.getClass().getSimpleName();        
        logger.error(type + " occured: ", exception);           
        return error(exception.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList()));
    }
    
    @ExceptionHandler({MissingServletRequestParameterException .class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handle(MissingServletRequestParameterException exception) {
        String type  = exception.getClass().getSimpleName();        
        logger.error(type + " occured: ", exception);                                
        ErrorResponse er = new ErrorResponse();
        er.setParameter(exception.getParameterName());
        er.setError(exception.getMessage());
        return er;
    }    
    
    @ExceptionHandler({MissingServletRequestPartException .class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handle(MissingServletRequestPartException exception) {
        String type  = exception.getClass().getSimpleName();        
        logger.error(type + " occured: ", exception);           
        ErrorResponse er = new ErrorResponse();
        er.setParameter(exception.getRequestPartName());
        er.setError(exception.getMessage());
        return er;
    }        
    
    @ExceptionHandler({HttpRequestMethodNotSupportedException .class})
    @ResponseBody
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity handle(HttpRequestMethodNotSupportedException exception) {
        String type  = exception.getClass().getSimpleName();        
        logger.error(type + " occured: ", exception);           
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
    }     
    
    @ExceptionHandler({HttpMediaTypeNotSupportedException .class})
    @ResponseBody
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    public ResponseEntity handle(HttpMediaTypeNotSupportedException exception) {
        String type  = exception.getClass().getSimpleName();        
        logger.error(type + " occured: ", exception);           
        return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build();
    }      
    
    @ExceptionHandler({TransactionSystemException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public ResponseEntity handle(TransactionSystemException exception) {
        logger.error("TransactionSystemException occured: ", exception.getMostSpecificCause());
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }      
    
    @ExceptionHandler({DataIntegrityViolationException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public ResponseEntity handle(DataIntegrityViolationException exception) {
        logger.error("DataIntegrityViolationException occured: ", exception.getMostSpecificCause());        
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }      
    
    @ExceptionHandler({HttpMessageNotReadableException.class, MethodArgumentTypeMismatchException.class, NumberFormatException.class, 
    IllegalArgumentException.class, NullPointerException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public ResponseEntity handle(Exception exception) {
        String type  = exception.getClass().getSimpleName();        
        logger.error(type + " occured: ", exception);      
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }       
    
    @ExceptionHandler({IOException.class, NestedServletException.class, ConversionNotSupportedException.class, HttpMessageNotWritableException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public ResponseEntity handleIOException(Exception exception) {
        String type  = exception.getClass().getSimpleName();        
        logger.error(type + " occured: ", exception);      
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }        

    @ExceptionHandler({ ServletRequestBindingException.class, TypeMismatchException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity handleServletRequestException(Exception exception) {
        String type  = exception.getClass().getSimpleName();        
        logger.error(type + " occured: ", exception);      
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }     
    

    private Map error(Object message) {
        return Collections.singletonMap("error", message);
    }        
    
    
}
