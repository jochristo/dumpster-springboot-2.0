package joch.armada.api.model;

import joch.armada.api.annotation.DateAttribute;
import joch.armada.api.domain.enumeration.PlacementStatus;

/**
 *
 * @author ic
 */
public class PlacementQuery
{    
    @DateAttribute
    private String dateIn;
    
    @DateAttribute
    private String createdAt;
    
    private double longitude;
    
    private double latitude;
    
    private PlacementStatus status;
    
}
