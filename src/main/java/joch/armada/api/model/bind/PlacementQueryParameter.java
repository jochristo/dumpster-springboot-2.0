package joch.armada.api.model.bind;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import joch.armada.api.Constants;
import joch.armada.api.annotation.DateAttribute;
import joch.armada.api.domain.enumeration.PlacementStatus;

/**
 *
 * @author Admin
 */
public class PlacementQueryParameter
{       
    @DateAttribute
    private String dateIn;
    
    @DateAttribute
    private String createdAt;
    
    @Min(value = -180, message=Constants.ErrorCodeMessage.INVALID_LONGITUDE_MESSAGE)
    @Max(value = 180, message=Constants.ErrorCodeMessage.INVALID_LONGITUDE_MESSAGE)
    private double longitude;
    
    @Min(value = -90, message=Constants.ErrorCodeMessage.INVALID_LATITUDE_MESSAGE)
    @Max(value = 90, message=Constants.ErrorCodeMessage.INVALID_LATITUDE_MESSAGE)    
    private double latitude;        
    
    private PlacementStatus status;        
        
    public String getDateIn() {
        return dateIn;
    }

    public void setDateIn(String dateIn) {
        this.dateIn = dateIn;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public PlacementStatus getStatus() {
        return status;
    }

    public void setStatus(PlacementStatus status) {
        this.status = status;
    }
    
    
    
}
