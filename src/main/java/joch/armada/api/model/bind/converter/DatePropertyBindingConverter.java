package joch.armada.api.model.bind.converter;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Admin
 */
public class DatePropertyBindingConverter extends PropertyEditorSupport
{
    private static final Logger logger = LoggerFactory.getLogger(DatePropertyBindingConverter.class);      
    
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        String upperCase = text.toUpperCase();   
        LocalDate ld = LocalDate.now();
        setValue(upperCase);
        /*
        try{
            ld = LocalDate.parse(upperCase, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            String temp = ld.toString();
        }
        catch (Exception ex){
            logger.error("IllegalArgumentException occured during date conversion... "+ ex.getMessage());   
            throw new InvalidArgumentValueException(ApiErrorCode.INVALID_PARAMETER_VALUE, "Given date format is invalid, use: 'dd-mm-yyyy'");
        }
        finally{
            setValue(ld);
        } 
        */
    }      
}
