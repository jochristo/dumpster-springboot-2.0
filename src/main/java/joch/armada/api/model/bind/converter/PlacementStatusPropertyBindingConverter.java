package joch.armada.api.model.bind.converter;

import java.beans.PropertyEditorSupport;
import joch.armada.api.domain.enumeration.PlacementStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Admin
 */
public class PlacementStatusPropertyBindingConverter extends PropertyEditorSupport
{
    private static final Logger logger = LoggerFactory.getLogger(PlacementStatusPropertyBindingConverter.class);      
    
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        String upperCase = text.toUpperCase();
        PlacementStatus mp = PlacementStatus.UNALLOCATED;
        try{
            mp = PlacementStatus.valueOf(upperCase);
            
        }
        catch (IllegalArgumentException ex){
            //logger.info("IllegalArgumentException occured during enum transform... "+ ex.getMessage());                             
        }
        finally{
            setValue(mp);
        }        
    }      
}
