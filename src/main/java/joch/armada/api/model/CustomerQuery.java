package joch.armada.api.model;

import java.util.HashSet;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.hibernate.validator.constraints.Email;
import static joch.armada.api.Constants.ErrorCode.INVALID_MOBILE_NO_ERROR_CODE;
import static joch.armada.api.Constants.ErrorCode.INVALID_NUMBER_GENERAL;
import static joch.armada.api.Constants.ErrorCodeMessage.INVALID_EMAIL_MESSAGE;
import static joch.armada.api.Constants.ErrorCodeMessage.INVALID_MOBILE_PHONE_MESSAGE;
import static joch.armada.api.Constants.ErrorCodeMessage.INVALID_PHONE_MESSAGE;
import joch.armada.api.annotation.NumericAttribute;
import joch.armada.api.exception.ParameterConstraintViolationException;
import joch.armada.data.mongo.predicate.PredicateOperation;

/**
 *
 * @author ic
 */
public class CustomerQuery
{
    @Email(message = INVALID_EMAIL_MESSAGE, regexp = "^[a-zA-Z0-9-_]+[@][a-z0-9A-Z_-]+[\\\\.][a-zA-Z]{2,3}$")      
    private String email;
    
    @NumericAttribute(pattern = NumericAttribute.AttributeType.MOBILE_NUMBER, errorCode = INVALID_MOBILE_NO_ERROR_CODE, message = INVALID_MOBILE_PHONE_MESSAGE)       
    private String mobileNo;
    
    @NumericAttribute(pattern = NumericAttribute.AttributeType.GENERAL_NUMBER, errorCode = INVALID_NUMBER_GENERAL, message = INVALID_PHONE_MESSAGE)        
    private String phoneNo;    
    
    private String lastname;       
        
    protected Set<Object> params = new HashSet();
        
    private PredicateOperation operation = PredicateOperation.EQUALS;    

    public CustomerQuery(String email, String mobileNo, String phoneNo, String lastname)
    {        
        this.email = email;        
        this.mobileNo = mobileNo;
        this.phoneNo = phoneNo;
        this.lastname = lastname;  
        
        this.validate();
    }        
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
        
    /**
     * Bean validation against custom annotation rules
     */
    protected void validate()
    {
        // bean validation
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<CustomerQuery>> violations = validator.validate(this);
        if (!violations.isEmpty()) {            
            throw new ParameterConstraintViolationException(violations);
        }         
    }     

    public PredicateOperation getOperation() {
        return operation;
    }

    public void setOperation(PredicateOperation operation) {
        this.operation = operation;
    }
    
    
}
