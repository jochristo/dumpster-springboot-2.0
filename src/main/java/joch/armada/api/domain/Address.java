package joch.armada.api.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.joko.core.jsonapi.annotations.JAPIAttribute;
import org.joko.core.jsonapi.annotations.JAPIType;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 *
 * @author ic
 */
@Document
@JAPIType(type = "Address")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Address
{
    @Field 
    @JAPIAttribute
    private String addressLine1;
    
    @Field 
    @JAPIAttribute
    private String addressLine2;
    
    @Field 
    @JAPIAttribute
    private String addressLine3;
    
    @Field     
    @JAPIAttribute
    private String contactName;
    
    @Field 
    @JAPIAttribute
    private String countryCode;
    
    @Field 
    @JAPIAttribute
    private String locality;
    
    @Field 
    @JAPIAttribute
    private String Organization;
    
    @Field 
    @JAPIAttribute
    private String postalCode;
    
    @Field 
    @JAPIAttribute
    private String region;
    
    @Field 
    @JAPIAttribute
    private String tag;

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getOrganization() {
        return Organization;
    }

    public void setOrganization(String Organization) {
        this.Organization = Organization;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    
    
}
