package joch.armada.api.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Min;
import joch.armada.api.Constants.EntityTypeName;
import joch.armada.api.exception.ParameterConstraintViolationException;
import org.joko.core.jsonapi.annotations.JAPIAttribute;
import org.joko.core.jsonapi.annotations.JAPIIdentifier;
import org.joko.core.jsonapi.annotations.JAPIType;
import org.joko.core.jsonapi.serializers.JsonDateFormatSerializer;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 *
 * @author ic
 */
@Document(collection = EntityTypeName.DUMPSTERS)
@JAPIType(type = EntityTypeName.DUMPSTERS)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(value = EntityTypeName.DUMPSTERS)
@JsonPropertyOrder({"available","allocated","reserved","total","createdAt","updatedAt"})
public class Dumpster implements Serializable
{        
    @Id
    @JAPIIdentifier
    private String id;
    
    @Field
    @JAPIAttribute
    @Min(value = 1, message = "The value of field 'total' must be greater than zero")
    private int total;
    
    @Field
    @JAPIAttribute    
    @Min(value = 0, message = "The value of field 'available' must be positive")
    private int available;
    
    @Field
    @JAPIAttribute    
    @Min(value = 0, message = "The value of field 'reserved' must be positive")
    private int reserved;
    
    @Field
    @JAPIAttribute    
    @Min(value = 0, message = "The value of field 'allocated' must be positive")
    private int allocated;
    
    @Field
    @JsonSerialize(using = JsonDateFormatSerializer.class)
    @JAPIAttribute    
    private Date createdAt = new Date();
    
    @Field
    @JsonSerialize(using = JsonDateFormatSerializer.class)
    @JAPIAttribute    
    private Date updatedAt;
    
    @Field
    @JAPIAttribute
    private boolean deleted = false;    

    public Dumpster newInstance()
    {        
        this.available = this.getTotal() - this.getAllocated() - this.getReserved();                        
        validate();                
        return this;
    }    
    
    public Dumpster updateInstance(Dumpster updated)
    {
        // != 0 : client provided input...
        if(updated.getTotal() != 0){
            this.total = updated.getTotal();
        }
        if(updated.getAllocated() != 0){
            this.allocated = updated.getAllocated();
        }       
        if(updated.getReserved()!= 0){
            this.reserved = updated.getReserved();
        }         

        this.available = this.getTotal() - this.getAllocated() - this.getReserved();
        this.setUpdatedAt(new Date());                
        validate();                
        return this;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    protected void validate()
    {
        // bean validation
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<Dumpster>> violations = validator.validate(this);
        if (!violations.isEmpty()) {            
            throw new ParameterConstraintViolationException(violations);
        }  
    }
    
    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getReserved() {
        return reserved;
    }

    public void setReserved(int reserved) {
        this.reserved = reserved;
    }

    public int getAllocated() {
        return allocated;
    }

    public void setAllocated(int allocated) {
        this.allocated = allocated;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    

    
}
