package joch.armada.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import joch.armada.api.Constants.EntityTypeName;
import joch.armada.api.exception.ParameterConstraintViolationException;
import org.joko.core.jsonapi.annotations.JAPIAttribute;
import org.joko.core.jsonapi.annotations.JAPIIdentifier;
import org.joko.core.jsonapi.annotations.JAPIRelationship;
import org.joko.core.jsonapi.annotations.JAPIType;
import org.joko.core.jsonapi.serializers.JsonDateFormatSerializer;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 *
 * @author ic
 */
@Document(collection = EntityTypeName.RESERVATIONS)
@JAPIType(type = EntityTypeName.RESERVATIONS)
@Entity(value = EntityTypeName.RESERVATIONS)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Reservation
{           
    
    @Id
    @Field
    @JAPIIdentifier
    @JsonIgnore
    private String id;
    
    @Field
    @JsonSerialize(using = JsonDateFormatSerializer.class)
    @JAPIAttribute    
    private Date createdAt = new Date();  
    
    @Field
    @JsonSerialize(using = JsonDateFormatSerializer.class)
    @JAPIAttribute        
    private Date updatedAt;    
    
    @Field      
    @JAPIRelationship(relationship = Placement.class, selfLink = "")        
    @DBRef(lazy = true, db = "db_dumpster_app")
    @NotNull(message = "placements relationships field is required")
    private List<Placement> placements;
    
    @Field      
    @JAPIRelationship(relationship = Customer.class, selfLink = "")     
    @DBRef(lazy = true, db = "db_dumpster_app")
    private Customer customer;        
    
    @Field
    @JAPIAttribute
    private boolean deleted = false;    
    
    /**
     * Updates current instance attributes with given object.
     * @param updated
     * @return 
     */
    public Reservation updateInstance(Reservation updated)
    {
        this.setCustomer(updated.getCustomer());
        this.setPlacements(updated.getPlacements());
        this.updatedAt = new Date();
        
        // validate
        this.validate();
        
        return this;
    }      
    
    public Reservation newInstance(){
        this.validate();
        return this;
    }
    
    /**
     * Bean validation against custom annotation rules
     */
    public void validate()
    {
        // bean validation
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<Reservation>> violations = validator.validate(this);
        if (!violations.isEmpty()) {            
            throw new ParameterConstraintViolationException(violations);
        }         
    }            
    
    public List<Placement> getPlacements() {
        return placements;
    }

    public void setPlacements(List<Placement> placements) {
        this.placements = placements;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
    
}
