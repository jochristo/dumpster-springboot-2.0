package joch.armada.api.domain.enumeration;

/**
 *
 * @author ic
 */
public enum PlacementStatus
{
    ONGOING(0, "ONGOING"), 
    
    ALLOCATED(1, "ALLOCATED"),    
    
    UNALLOCATED(2, "UNALLOCATED"),    
    
    RETURNED(3, "RETURNED"),    
    
    CANCELLED(4, "CANCELLED")    
    ;
    
    private final int value;
    private final String label;
    private PlacementStatus(int x, String label)
    {
        this.value = x;
        this.label = label;
    }
    
    public String getType()
    {
        return this.label;
    }
    
    public int getValue()
    {
        return this.value;
    }    
}
