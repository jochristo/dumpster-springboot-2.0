package joch.armada.api.domain;

import joch.armada.api.domain.enumeration.PlacementStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import joch.armada.api.Constants.EntityTypeName;
import static joch.armada.api.Constants.ErrorCode.INVALID_LATITUDE_ERROR_CODE;
import static joch.armada.api.Constants.ErrorCode.INVALID_LONGITUDE_ERROR_CODE;
import static joch.armada.api.Constants.ErrorCodeMessage.INVALID_LATITUDE_MESSAGE;
import static joch.armada.api.Constants.ErrorCodeMessage.INVALID_LONGITUDE_MESSAGE;
import joch.armada.api.annotation.NumericAttribute;
import static joch.armada.api.annotation.NumericAttribute.AttributeType.LATITUDE;
import static joch.armada.api.annotation.NumericAttribute.AttributeType.LONGITUDE;
import joch.armada.api.exception.ParameterConstraintViolationException;
import org.joko.core.jsonapi.annotations.JAPIAttribute;
import org.joko.core.jsonapi.annotations.JAPIIdentifier;
import org.joko.core.jsonapi.annotations.JAPIType;
import org.joko.core.jsonapi.serializers.JsonDateFormatSerializer;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 *
 * @author ic
 */
@Document(collection = EntityTypeName.PLACEMENTS)
@JAPIType(type = EntityTypeName.PLACEMENTS)
@Entity(value = EntityTypeName.PLACEMENTS)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Placement implements Serializable
{
 
    @Id
    @JAPIIdentifier
    @JsonIgnore    
    private String id;
    
    @Field
    @JsonSerialize(using = JsonDateFormatSerializer.class)
    @JAPIAttribute    
    private Date dateIn = new Date();
    
    @Field
    @JAPIAttribute
    private String dateOut;    
    
    @Field
    @Indexed(unique = true)
    @NotNull(message = "Dumpster reference id field is mandatory")
    @JAPIAttribute
    private String dumpsterReferenceId = UUID.randomUUID().toString();
    
    @Field
    @JAPIAttribute    
    @NumericAttribute(pattern = LONGITUDE, errorCode = INVALID_LONGITUDE_ERROR_CODE, message = INVALID_LONGITUDE_MESSAGE)
    private double longitude;
    
    @Field
    @JAPIAttribute
    @NumericAttribute(pattern = LATITUDE, errorCode = INVALID_LATITUDE_ERROR_CODE, message = INVALID_LATITUDE_MESSAGE)
    private double latitude;
    
    //@Field
    //@JAPIAttribute    
    private Address address;    
    
    @Field
    @NotNull(message = "Status field is mandatory")
    @JAPIAttribute
    private PlacementStatus status = PlacementStatus.UNALLOCATED;
        
    @Field
    @JsonSerialize(using = JsonDateFormatSerializer.class)
    @JAPIAttribute    
    private Date createdAt = new Date();     
    
    @Field
    @JsonSerialize(using = JsonDateFormatSerializer.class)
    @JAPIAttribute    
    private Date updatedAt;
    
    @Field
    @JAPIAttribute
    private boolean deleted = false;
       
       
    /**
     * Creates new instance with default attributes.
     * @return 
     */
    public Placement newInstance()
    {
        this.dumpsterReferenceId = UUID.randomUUID().toString();                        
        this.validate();        
        return this;
    }    
    
    /**
     * Updates current instance attributes with given object.
     * @param updated
     * @return 
     */
    public Placement updateInstance(Placement updated)
    {
        this.setAddress(updated.getAddress());
        this.setDateOut(updated.getDateOut());
        this.setLatitude(updated.getLatitude());
        this.setLongitude(updated.getLongitude());
        this.setStatus(updated.getStatus());
        this.setUpdatedAt(new Date());

        // validate
        this.validate();
        
        return this;
    }      
    
    /**
     * Bean validation against custom annotation rules
     */
    public void validate()
    {
        // bean validation
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<Placement>> violations = validator.validate(this);
        if (!violations.isEmpty()) {            
            throw new ParameterConstraintViolationException(violations);
        }         
    }   

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.id);
        hash = 41 * hash + Objects.hashCode(this.dumpsterReferenceId);
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.longitude) ^ (Double.doubleToLongBits(this.longitude) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.latitude) ^ (Double.doubleToLongBits(this.latitude) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Placement other = (Placement) obj;
        return Objects.equals(this.id, other.id);
    }

    public String getId() {
        return id;
    }

    public String getDumpsterReferenceId() {
        return dumpsterReferenceId;
    }       
        
    public Date getDateIn() {
        return dateIn;
    }

    public void setDateIn(Date dateIn) {
        this.dateIn = dateIn;
    }

    public String getDateOut() {
        return dateOut;
    }

    public void setDateOut(String dateOut) {
        this.dateOut = dateOut;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public PlacementStatus getStatus() {
        return status;
    }

    public void setStatus(PlacementStatus status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
    
    
    
}
