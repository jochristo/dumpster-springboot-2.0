package joch.armada.api.domain.converter;

import joch.armada.api.domain.enumeration.PlacementStatus;
import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author ic
 */
public class PlacementStatusConverter implements Converter<PlacementStatus, String>
{

    @Override
    public String convert(PlacementStatus s) {
        return s.getType();
    }
    
}
