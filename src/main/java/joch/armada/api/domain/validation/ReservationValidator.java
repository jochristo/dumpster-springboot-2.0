package joch.armada.api.domain.validation;

import joch.armada.api.domain.Reservation;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Admin
 */
public class ReservationValidator implements Validator
{

    @Override
    public boolean supports(Class<?> type) {
        return Reservation.class.equals(type);
    }

    @Override
    public void validate(Object object, Errors errors) {
        //ValidationUtils.rejectIfEmpty(errors, "name", "name.empty");
        Reservation reservation = (Reservation) object;
        if (reservation.getPlacements() == null) {
            //errors.rejectValue("placements", "placements data is required");
            errors.rejectValue("placements", "empty field", new Object[]{"placements"}, "placements data is not present in reservation");
        }         
    }
    
}
