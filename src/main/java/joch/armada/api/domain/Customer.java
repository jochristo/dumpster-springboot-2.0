package joch.armada.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Email;
import joch.armada.api.Constants.EntityTypeName;
import static joch.armada.api.Constants.ErrorCode.INVALID_MOBILE_NO_ERROR_CODE;
import static joch.armada.api.Constants.ErrorCode.INVALID_NUMBER_GENERAL;
import static joch.armada.api.Constants.ErrorCodeMessage.INVALID_EMAIL_MESSAGE;
import static joch.armada.api.Constants.ErrorCodeMessage.INVALID_MOBILE_PHONE_MESSAGE;
import static joch.armada.api.Constants.ErrorCodeMessage.INVALID_PHONE_MESSAGE;
import joch.armada.api.annotation.NumericAttribute;
import joch.armada.api.annotation.NumericAttribute.AttributeType;
import joch.armada.api.exception.ParameterConstraintViolationException;
import org.joko.core.jsonapi.annotations.JAPIAttribute;
import org.joko.core.jsonapi.annotations.JAPIIdentifier;
import org.joko.core.jsonapi.annotations.JAPIType;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Customer entity/document
 * @author ic
 */
@Document(collection = EntityTypeName.CUSTOMERS)
@JAPIType(type = EntityTypeName.CUSTOMERS)
@Entity(value = EntityTypeName.CUSTOMERS)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Customer implements Serializable
{
    
    @Id
    @JAPIIdentifier
    @JsonIgnore
    private String id;
    
    @Field
    @JAPIAttribute
    @NotNull
    private String lastname;
    
    @Field
    @JAPIAttribute
    @NotNull
    private String firstname;
    
    @Field
    @Indexed(unique = true)
    @JAPIAttribute
    @Email(message = INVALID_EMAIL_MESSAGE, regexp = "^[a-zA-Z0-9-_]+[@][a-z0-9A-Z_-]+[\\\\.][a-zA-Z]{2,3}$")
    @NotNull
    private String email;       
    
    @Field
    @JAPIAttribute
    @NumericAttribute(pattern = AttributeType.MOBILE_NUMBER, errorCode = INVALID_MOBILE_NO_ERROR_CODE, message = INVALID_MOBILE_PHONE_MESSAGE)
    @NotNull
    private String mobileNo;
    
    @Field
    @JAPIAttribute
    @NumericAttribute(pattern = AttributeType.GENERAL_NUMBER, errorCode = INVALID_NUMBER_GENERAL, message = INVALID_PHONE_MESSAGE)
    private String phoneNo;
    
    @Field
    @JAPIAttribute
    private String notes;
    
    @Field
    @JAPIAttribute
    private boolean deleted = false;    

    public Customer newInstance()
    {
        this.validate();
        return this;
    }
    
    public Customer updateInstance(Customer updated)
    {
        this.setEmail(updated.getEmail());
        this.setFirstname(updated.getFirstname());
        this.setLastname(updated.getLastname());
        this.setMobileNo(updated.getMobileNo());
        this.setNotes(updated.getNotes());
        this.setPhoneNo(updated.getPhoneNo());
        
        this.validate();
        
        return this;
    }
    
    /**
     * Bean validation against custom annotation rules
     */
    protected void validate()
    {
        // bean validation
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<Customer>> violations = validator.validate(this);
        if (!violations.isEmpty()) {            
            throw new ParameterConstraintViolationException(violations);
        }         
    }     
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }    
    
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
    
}
