package joch.armada.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;
import static joch.armada.api.Endpoints.APPLICATION_CODES;
import static joch.armada.api.Endpoints.DUMPSTERS;
import static joch.armada.api.Endpoints.DUMPSTERS_BY_ID;
import joch.armada.api.Constants;
import static joch.armada.api.Endpoints.CUSTOMERS;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.api.error.Utils;
import joch.armada.api.exception.ResourceNotFoundException;
import joch.armada.api.model.CustomerQuery;
import joch.armada.api.domain.Dumpster;
import joch.armada.api.service.ICustomerService;
import joch.armada.api.service.IDumpsterService;
import joch.armada.data.mongo.predicate.PredicateOperation;
import org.joko.core.jsonapi.JSONApiData;
import org.joko.core.jsonapi.JSONApiDocument;
import org.joko.core.utilities.Utilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ic
 */
@RestController
@RequestMapping(value = "/internal")
public class InternalApiController
{        
    @Autowired 
    private IDumpsterService dumpsterService; 
    
    @Autowired 
    private ICustomerService customerService;     
    
    private static final Logger logger = LoggerFactory.getLogger(InternalApiController.class);   
    
    @RequestMapping(value = APPLICATION_CODES, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @ResponseBody
    public JSONApiDocument getAppCodes()
    {
        List codes = Utils.applicationCodes();
        logger.info("Acquiring application-specific error codes", codes);
        return new JSONApiDocument(codes);
    }    
    
    @RequestMapping(value = DUMPSTERS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @ResponseBody
    public JSONApiDocument getAllDumpsterInfo(@RequestParam(name = "query", required = false) String query)
    {        
        logger.info("Acquiring stored dumpster information...");
        List<Dumpster> list = dumpsterService.get();    
        logger.info("Done.");
        return new JSONApiDocument(list);
    }     
    
    @RequestMapping(value = DUMPSTERS_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @ResponseBody
    public JSONApiDocument getDumpsterInfo(@PathVariable @NotNull String id)
    {
        logger.info("Acquiring stored dumpster information...");
        Dumpster dumpsterInfo = dumpsterService.findOne("id", id);      
        if (dumpsterInfo == null) {
            throw new ResourceNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Dumpster id not found: " + id);
        }            
        logger.info("Done.");
        return new JSONApiDocument(dumpsterInfo);
    }     
    
    @RequestMapping(value = DUMPSTERS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST )
    @ResponseBody
    public JSONApiDocument createDumpster(@RequestBody JSONApiDocument document)
    {
        JSONApiData data = (JSONApiData) document.getData();
        Dumpster dumpsterInfo = (Dumpster) data.convertTo(Dumpster.class);                            
        List<Dumpster> list = dumpsterService.get();
        Dumpster query = null;
        if (list.isEmpty() == false) {
            logger.info("Loading dumpster info document from MongoDB...");
            //Dumpster element = dumpsterService.findOne(list.get(0).getId());
            Dumpster element = list.get(0);
            logger.info("Updating dumpster info document...");             
            element = element.updateInstance(dumpsterInfo);
            dumpsterService.save(element);
            logger.info("Loading dumpster info document...");
            query = element;            
        }       
        else{
            logger.info("Creating/saving new dumpster info document..."); 
            dumpsterService.save(dumpsterInfo.newInstance());       
            logger.info("Loading dumpster info document...");
            query = dumpsterService.findOne("id",dumpsterInfo.getId());
        }
        logger.info("Done.");
        return new JSONApiDocument(query);
    }  

    @RequestMapping(value = DUMPSTERS_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.PUT )
    @ResponseBody
    public JSONApiDocument postDumpster(@PathVariable @NotNull String id, @RequestBody JSONApiDocument document)
    {
        JSONApiData data = (JSONApiData) document.getData();
        Dumpster dumpsterInfo = (Dumpster) data.convertTo(Dumpster.class);                                    
        Dumpster query = dumpsterService.findOne("id", id);
        if (null != query) {            
            logger.info("Loading dumpster info document from MongoDB...");
            logger.info("Updating dumpster info document to MongoDB...");             
            query = query.updateInstance(dumpsterInfo);
            dumpsterService.update(id, query);
            logger.info("Loading dumpster info document from MongoDB...");            
        }       
        else{
            throw new ResourceNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Dumpster id not found: " + id);
        }
        logger.info("Done.");
        return new JSONApiDocument(query);
    }     
    
    @RequestMapping(value = DUMPSTERS + "z", produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @ResponseBody    
    public JSONApiDocument dummy
        (        
        @RequestParam(value = "allocated", required = false) String allocated,
        @RequestParam(value = "available", required = false) String available,
        @RequestParam(value = "reserved", required = false) String reserved,
        @RequestParam(value = "min", required = false) Object min,
        @RequestParam(value = "max", required = false) Object max        
    )
    {        
        Map<Object,Object> requestMap = new HashMap();        
        if(!Utilities.isEmpty(allocated)){
            requestMap.put("allocated", allocated);        
            logger.info("Requested allocated: " + allocated);
            
            if(!Utilities.isEmpty(min) && !Utilities.isEmpty(max))
            {
                Map<Object,Object> values = new HashMap();
                values.put("min", min);
                values.put("max", max);
                requestMap.put("allocated", values);        
                logger.info("Requested min: " + min);
                logger.info("Requested max: " + max);
            } 
           
        } 
        if(!Utilities.isEmpty(available)){
            requestMap.put("available", available);        
            logger.info("Requested available: " + available);
        } 
        if(!Utilities.isEmpty(reserved)){
            requestMap.put("reserved", reserved);        
            logger.info("Requested reserved: " + reserved);
        } 
       
        
        return new JSONApiDocument(dumpsterService.find(requestMap, PredicateOperation.BETWEEN));               
    }
    
    @RequestMapping(value = CUSTOMERS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @ResponseBody    
    public JSONApiDocument getCustomer(        
        @RequestParam(value = "email", required = false) String email,
        @RequestParam(value = "mobileNo", required = false) String mobileNo,
        @RequestParam(value = "phoneNo", required = false) String phoneNo,
        @RequestParam(value = "lastname", required = false) String lastname
    )
    {        
        Map<Object,Object> requestMap = new HashMap();        
        CustomerQuery customerQuery = new CustomerQuery(email, mobileNo, phoneNo, lastname);     
        requestMap.put("email", email);
        logger.info("Requested email: " + email);
        requestMap.put("mobileNo", mobileNo);
        logger.info("Requested mobileNo: " + mobileNo);
        requestMap.put("phoneNo", phoneNo);
        logger.info("Requested phoneNo: " + phoneNo);
        requestMap.put("lastname", lastname);
        logger.info("Requested lastname: " + lastname);       
        
        if(!Utilities.isEmpty(requestMap)){            

            return new JSONApiDocument(customerService.find(requestMap));
        } 
               
        return new JSONApiDocument(customerService.get());
    }    
    
}
