package joch.armada.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;
import static joch.armada.api.Endpoints.CUSTOMERS;
import static joch.armada.api.Endpoints.CUSTOMERS_BY_ID;
import joch.armada.api.Constants;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.api.exception.ResourceIdentifierNotFoundException;
import joch.armada.api.exception.ResourceNotFoundException;
import joch.armada.api.domain.Customer;
import joch.armada.api.service.ICustomerService;
import org.joko.core.jsonapi.JSONApiData;
import org.joko.core.jsonapi.JSONApiDocument;
import org.joko.core.utilities.Utilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ic
 */
@RestController
public class CustomersController
{
    private static final Logger logger = LoggerFactory.getLogger(CustomersController.class); 
    
    @Autowired 
    private ICustomerService customerService;    
       
    @RequestMapping(value = CUSTOMERS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)
    @ResponseBody
    public JSONApiDocument createCustomer(@RequestBody JSONApiDocument document)
    {          
        JSONApiData data = (JSONApiData) document.getData();
        Customer customer = (Customer) data.convertTo(Customer.class);                        
        logger.info("Saving customer document to MongoDB...");                        
        customerService.insert(customer.newInstance());
        logger.info("Done.");
        logger.info("Loading customer document from MongoDB...");
        Customer c = customerService.findOne(customer.getId());        
        return new JSONApiDocument(c);
    }          
    
    @RequestMapping(value = CUSTOMERS_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody
    public JSONApiDocument getCustomer(@PathVariable @NotNull String id)
    {         
        logger.info("Querying MongoDB to find customer information id..." + id);                        
        Customer c = customerService.findOne(id);        
        if(c == null){
            throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Customer id not found: " + id);
        }
        logger.info("Document found");
        return new JSONApiDocument(c);
    }         
    
    @RequestMapping(value = CUSTOMERS_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.PUT)  
    @ResponseBody
    public JSONApiDocument updateCustomer(@PathVariable @NotNull String id, @RequestBody JSONApiDocument document)
    {       
        /*
        logger.info("Querying MongoDB to find customer information id..." + id);                                             
        Customer c = customerService.findOneObject("id", id);            
        if(c == null){
            throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Customer id not found: " + id);
        }         
        logger.info("Document found");          
        */
        JSONApiData data = (JSONApiData) document.getData();
        Customer customer = (Customer) data.convertTo(Customer.class);                      
        //customerService.update(id,c.updateInstance(customer));
        logger.info("Querying MongoDB to find customer information id..." + id);   
        Customer c = customerService.update(id,customer);
        logger.info("Document found");
        logger.info("Document updated");
        return new JSONApiDocument(c);
    }      
    
    @RequestMapping(value = CUSTOMERS_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.DELETE)  
    @ResponseBody
    public JSONApiDocument deleteCustomer(@PathVariable @NotNull String id)
    {         
        logger.info("Querying MongoDB to find customer information id..." + id);                        
        Customer c = customerService.findOne("id",id);        
        if(c == null){
            throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Customer id not found: " + id);
        }        
        logger.info("Document found");
        customerService.delete(c);
        logger.info("Document deleted");
        return new JSONApiDocument(c);
    }         
       
    @RequestMapping(value = CUSTOMERS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody
    public JSONApiDocument getCustomers(
        @RequestParam(value = "email", required = false) String email,
        @RequestParam(value = "mobileNo", required = false) String mobileNo,
        @RequestParam(value = "phoneNo", required = false) String phoneNo,
        @RequestParam(value = "lastname", required = false) String lastname,
        @RequestParam(value = "min", required = false) Object min,
        @RequestParam(value = "max", required = false) Object max,
        @RequestParam(value = "operation", required = false) String operation
        
    )
    {         
        Map<Object,Object> requestMap = new HashMap<>();
        if(!Utilities.isEmpty(email)){
            requestMap.put("email", email);        
            logger.info("Requested email: " + email);
        }
        if(!Utilities.isEmpty(mobileNo)){
            String temp = new String(mobileNo);
            StringBuilder sb = new StringBuilder();
            String trimmed = temp.trim();
            if(!trimmed.startsWith("+")){
                sb.append("+");
                sb.append(trimmed);
            }
            trimmed = sb.toString();
            requestMap.put("mobileNo", trimmed);        
            logger.info("Requested mobileNo: " + trimmed);
        }
        if(!Utilities.isEmpty(phoneNo)){
            requestMap.put("phoneNo", phoneNo.trim());        
            logger.info("Requested phoneNo: " + phoneNo.trim());
        }
        if(!Utilities.isEmpty(lastname)){
            requestMap.put("lastname", lastname);        
            logger.info("Requested lastname: " + lastname);
        }        
        
        if(requestMap.size() > 0)
        {
            return new JSONApiDocument(customerService.find(requestMap));
        }
                
        logger.info("Querying MongoDB to find all customer information...");                        
        List<Customer> customers = customerService.get();
        if(customers == null){
            throw new ResourceNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Customer not found");
        }
        logger.info("Document(s) found");
        return new JSONApiDocument(customers);
    }      
}
