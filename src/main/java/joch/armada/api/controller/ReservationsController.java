package joch.armada.api.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import static joch.armada.api.Endpoints.RESERVATIONS;
import static joch.armada.api.Endpoints.RESERVATION_BY_ID;
import joch.armada.api.Constants;
import joch.armada.api.Constants.JSONApiRelationshipName;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.api.exception.ResourceIdentifierNotFoundException;
import joch.armada.api.exception.ResourceNotFoundException;
import joch.armada.core.exception.GeneralApplicationCoreException;
import joch.armada.data.mongo.component.ITransactionalEntityHandler;
import joch.armada.api.domain.Customer;
import joch.armada.api.domain.Placement;
import joch.armada.api.domain.Reservation;
import joch.armada.api.handler.IFieldValidationHandler;
import joch.armada.api.service.IPlacementService;
import joch.armada.api.service.IReservationService;
import joch.armada.core.exception.InvalidArgumentValueException;
import joch.armada.data.mongo.service.IMorphiaService;
import joch.armada.data.redis.service.IRedisHashService;
import org.joko.core.jsonapi.JSONApiData;
import org.joko.core.jsonapi.JSONApiDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ic
 */
@RestController
public class ReservationsController
{
    private static final Logger logger = LoggerFactory.getLogger(ReservationsController.class);      

    @Autowired @Qualifier("reservationService") private IReservationService reservationService;        
    @Autowired @Qualifier("placementService")  private IPlacementService placementService;         
    @Autowired private IRedisHashService redis;    
    @Autowired private ITransactionalEntityHandler<Reservation> transactional;    
    @Autowired private IMorphiaService morphiaService;        
    @Autowired private IFieldValidationHandler fieldValidationHandler;           
    
    
    @InitBinder
    protected void initBinder(final WebDataBinder webdataBinder) {
        //this.webdataBinder = webdataBinder;
        //this.webdataBinder.addValidators(new ReservationValidator());
        
        /*
        DataBinder binder = new DataBinder(reservation);
        binder.addValidators(new ReservationValidator());
        binder.validate(reservation);
        // get BindingResult that includes any validation errors
        BindingResult results = binder.getBindingResult();        
        // field validation
        if(results.hasErrors()){            
            fieldValidationHandler.resolveErrors(results.getFieldErrors());                        
        }        
        */        
        
    }    
    
       
    @RequestMapping(value = RESERVATIONS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)
    @ResponseBody
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public JSONApiDocument createReservation(@RequestBody JSONApiDocument document)
    {                  
        
        JSONApiData data = (JSONApiData) document.getData();                
        Reservation reservation = (Reservation) data.convertTo(Reservation.class);                                         
        List<JSONApiData> included = (List<JSONApiData>) document.getIncluded();                                
        List<Placement> placements = new ArrayList();
        
        // check body contains included "placements"
        Predicate<JSONApiData> predicate = p -> p.getType().equals(JSONApiRelationshipName.PLACEMENTS_RELATIONSHIP);
        if (!included.stream().anyMatch(predicate)){
            throw new InvalidArgumentValueException(ApiErrorCode.INVALID_PARAMETER_VALUE, "included data of type 'placements' is required");
        }
                
        // iterate over relationships data
        included.stream().map((relationship) -> {            
            if (relationship.getType().equals(JSONApiRelationshipName.CUSTOMERS_RELATIONSHIP)) {
                Customer customer = (Customer) relationship.convertTo(Customer.class);
                // set customer relationship value
                if (customer != null) {
                    reservation.setCustomer(customer);
                }
            }
            return relationship;
        }).filter((relationship) -> (relationship.getType().equals(JSONApiRelationshipName.PLACEMENTS_RELATIONSHIP)))
          .map((relationship) -> (Placement) relationship.convertTo(Placement.class))
          .filter((pl) -> (pl != null)).forEachOrdered((pl) -> 
        {
            //validate first
            pl.validate();
            
            Placement placement  = new Placement().newInstance();            
            placementService.save(placement.updateInstance(pl));
            
            // add to cache
            String hash = "Placement/" + placement.getId();
            redis.hAdd(hash, placement.getId(), placement, 10);            
            placements.add(placement);
        });        
        
        // set placements relationship value
        if(placements.isEmpty() == false){
            reservation.setPlacements(placements);
        }
        
        Reservation res = new Reservation();
        try
        {                        
            logger.info("Saving reservation document...");                        
            reservationService.save(reservation);
            logger.info("Done.");        
            logger.info("Loading reservation document...");
            res = reservationService.findOne("id", reservation.getId());             
        }
        catch(Exception ex){
            placements.stream().forEachOrdered((pl)->
            {
                if(redis.hasKey("Placement/"+pl.getId())){
                    placementService.delete(pl);
                }
            });
            throw new GeneralApplicationCoreException(ApiErrorCode.INTERNAL_SERVER_ERROR, ex.getMessage());
        }        
      
        return new JSONApiDocument(res);
    }       
    
    @RequestMapping(value = RESERVATION_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody
    public JSONApiDocument getReservation(@PathVariable @NotNull(message = "Reservation id is missing") String id)
    {         
        logger.info("Querying MongoDB to find reservation information id..." + id);                        
        Reservation reservation = reservationService.findOne(id);        
        if(reservation == null){
            throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Reservation id not found: " + id);
        }
        logger.info("Document found");
        return new JSONApiDocument(reservation);
    }         
    
    @RequestMapping(value = RESERVATION_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.PUT)  
    @ResponseBody
    public JSONApiDocument updateReservation(@PathVariable @NotNull(message = "Reservation id is missing") String id, @RequestBody @Valid Reservation reservation)
    {                
        logger.info("Querying MongoDB to find reservation information id..." + id);                        
        Reservation res = reservationService.findOne(id);        
        if(res == null){
            throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Reservation id not found: " + id);
        }        
        logger.info("Document found");        
        reservationService.update(id,reservation);
        logger.info("Document updated");
        return new JSONApiDocument(reservation);
    }      
    
    @RequestMapping(value = RESERVATION_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.DELETE)  
    @ResponseBody
    public JSONApiDocument deleteReservation(@PathVariable @NotNull(message = "Reservation id is missing") String id)
    {         
        logger.info("Querying MongoDB to find reservation information id..." + id);                        
        Reservation res = reservationService.findOne(id);        
        if(res == null){
            throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Reservation id not found: " + id);
        }        
        logger.info("Document found");
        reservationService.delete(res);
        logger.info("Document deleted");
        return new JSONApiDocument(res);
    }         
       
    @RequestMapping(value = RESERVATIONS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody
    public JSONApiDocument getReservations(@RequestParam(value = "operation", required = false, defaultValue = "EQ") String operation)
    {         
        logger.info("Querying MongoDB to find all reservation documents...");
        List<Reservation> reservations = reservationService.get();
        if(reservations == null){
            throw new ResourceNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Reservations not found");
        }
        logger.info("Document(s) found");
        
        return new JSONApiDocument(reservations);
    }    
    
    @RequestMapping(value = RESERVATIONS + "z", produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)
    @ResponseBody
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public JSONApiDocument createReservationZZZZ(@RequestBody JSONApiDocument document)
    {                  
        JSONApiData data = (JSONApiData) document.getData();                
        Reservation reservation = (Reservation) data.convertTo(Reservation.class);                        
        List<JSONApiData> included = (List<JSONApiData>) document.getIncluded();        
        List<Placement> placements = new ArrayList();
             
        Collection<Object> collection = new ArrayList();
        
        // iterate over relationships data
        included.stream().map((relationship) -> {
            if (relationship.getType().equals(JSONApiRelationshipName.CUSTOMERS_RELATIONSHIP)) {
                Customer customer = (Customer) relationship.convertTo(Customer.class);
                // set customer relationship value
                if (customer != null) {
                    reservation.setCustomer(customer);
                    collection.add(customer);
                }
            }
            return relationship;
        }).filter((relationship) -> (relationship.getType().equals(JSONApiRelationshipName.PLACEMENTS_RELATIONSHIP)))
          .map((relationship) -> (Placement) relationship.convertTo(Placement.class))
          .filter((pl) -> (pl != null)).forEachOrdered((pl) -> 
        {
            Placement placement  = new Placement().newInstance();      
            collection.add(placement);
            //placementService.save(placement.updateInstance(pl));
            
            // add to cache
            //String hash = "Placement/" + placement.getId();
            //redis.hAdd(hash, placement.getId(), placement, 10);            
            placements.add(placement);
        }); 
        
        reservation.setPlacements(placements);
        //Reservation res = new Reservation();        
        return new JSONApiDocument(transactional.save(reservation, collection));
    }     
    
}
