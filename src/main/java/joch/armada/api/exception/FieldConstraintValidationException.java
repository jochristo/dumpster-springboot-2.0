package joch.armada.api.exception;

import java.util.List;
import joch.armada.api.error.ErrorResponse;

/**
 *
 * @author ic
 */
public class FieldConstraintValidationException extends RuntimeException
{
    private final List<ErrorResponse> errors;    
        
    public FieldConstraintValidationException(List<ErrorResponse> errors) {
        
        super("Field(s) contain invalid or no data. Found: " + errors.size());
        this.errors = errors;
    }    
    
    public List<ErrorResponse> getErrors() {
        return errors;
    }
    
    
    
}
