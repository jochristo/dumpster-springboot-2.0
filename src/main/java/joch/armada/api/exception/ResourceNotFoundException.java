package joch.armada.api.exception;

import joch.armada.api.error.ApiError;
import joch.armada.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class ResourceNotFoundException extends RestApplicationException
{
    public ResourceNotFoundException(String details) {
        super(details);
        super.setApiErrorCode(ApiErrorCode.RESOURCE_NOT_FOUND);
    }

    public ResourceNotFoundException(ApiError apiError, String details) {
        super(apiError, details);        
    }

    public ResourceNotFoundException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
        
    }
    
    
}
