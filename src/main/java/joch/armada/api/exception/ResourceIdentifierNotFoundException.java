package joch.armada.api.exception;

import joch.armada.api.error.ApiError;
import joch.armada.api.error.ApiErrorCode;


/**
 *
 * @author ic
 */
public class ResourceIdentifierNotFoundException extends RestApplicationException
{
    public ResourceIdentifierNotFoundException(String details) {
        super(details);
        super.setApiErrorCode(ApiErrorCode.RESOURCE_NOT_FOUND);
    }

    public ResourceIdentifierNotFoundException(ApiError apiError, String details) {
        super(apiError, details);        
    }

    public ResourceIdentifierNotFoundException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
        
    }
    
    
}
