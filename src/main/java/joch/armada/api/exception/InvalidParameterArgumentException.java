package joch.armada.api.exception;


import joch.armada.api.error.ApiError;
import joch.armada.api.error.ApiErrorCode;
import org.springframework.http.HttpStatus;

/**
 *
 * @author ic
 */
public class InvalidParameterArgumentException extends RestApplicationException
{

    public InvalidParameterArgumentException(String details) {
        super(details);
        super.setApiErrorCode(ApiErrorCode.INVALID_PARAMETER_VALUE);
    }

    public InvalidParameterArgumentException(HttpStatus httpStatus, ApiError apiError, String details) {
        super(httpStatus, apiError, details);
    }

    public InvalidParameterArgumentException(ApiError apiError, String details) {
        super(apiError, details);
    }

    public InvalidParameterArgumentException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }
    
    
    
    
}
