package joch.armada.api.exception;

import joch.armada.api.error.ApiError;
import joch.armada.api.error.ApiErrorCode;
import org.springframework.http.HttpStatus;

/**
 *
 * @author ic
 */
public class MissingRequestParameterException extends RestApplicationException
{

    public MissingRequestParameterException(String details) {
        super(details);
        super.setApiErrorCode(ApiErrorCode.MISSING_REQUEST_PARAMETER);
    }

    public MissingRequestParameterException(HttpStatus httpStatus, ApiError apiError, String details) {
        super(httpStatus, apiError, details);
    }

    public MissingRequestParameterException(ApiError apiError, String details) {
        super(apiError, details);
    }       

    public MissingRequestParameterException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }
    
    
    
}
