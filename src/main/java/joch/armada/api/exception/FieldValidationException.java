package joch.armada.api.exception;

import java.util.List;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.api.error.ErrorResponse;


/**
 *
 * @author ic
 */
public class FieldValidationException extends RuntimeException
{
    protected ApiErrorCode apiErrorCode;
    protected ErrorResponse errorResponse;
    private List<ErrorResponse> errorResponses;
    
    public FieldValidationException(String details) {
        super(details);
    }
    
    public FieldValidationException(ErrorResponse errorResponse){
        super(errorResponse.getError());
        this.apiErrorCode = ApiErrorCode.INVALID_PARAMETER_VALUE;
        this.errorResponse = errorResponse;
    }

    public FieldValidationException(List<ErrorResponse> errorResponses){
        super(ApiErrorCode.INVALID_PARAMETER_VALUE.getTitle());
        this.apiErrorCode = ApiErrorCode.INVALID_PARAMETER_VALUE;
        this.errorResponses = errorResponses;
    }    
    
    public ApiErrorCode getApiErrorCode() {
        return apiErrorCode;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    public List<ErrorResponse> getErrorResponses() {
        return errorResponses;
    }
    
    
    
}
