package joch.armada.api.exception;

import joch.armada.api.error.ApiError;
import joch.armada.api.error.ApiErrorCode;
import org.springframework.http.HttpStatus;

/**
 * Main REST application exception class for all custom exceptions in this app.
 *
 * @author ic
 */
public abstract class RestApplicationException extends RuntimeException
{
    private static final long serialVersionUID = -7498621923421702089L;
    protected String appCode;
    protected String details;
    protected HttpStatus httpStatus;    
    protected String title;    

    protected ApiErrorCode apiErrorCode;
    
    public RestApplicationException() {
    }

    public RestApplicationException(String message) {
        super(message);
    }

    public RestApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RestApplicationException(Throwable cause) {
        super(cause);
    }

    public RestApplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public RestApplicationException(ApiError apiError) {
        super(apiError.getDetails());
        this.appCode = apiError.getAppCode();
        this.details = apiError.getDetails();
    }

    public RestApplicationException(String appCode, String details) {
        super(details);
        this.appCode = appCode;
        this.details = details;
    }

    public RestApplicationException(HttpStatus httpStatus, ApiError apiError, String details) {
        super(details);
        this.appCode = apiError.getAppCode();
        this.details = details;
        this.title = apiError.getDetails();
        this.httpStatus = httpStatus;
    }
    
    public RestApplicationException(ApiError apiError, String details) {
        super(details);
        this.appCode = apiError.getAppCode();
        this.details = apiError.getDetails();  
        this.title = apiError.getDetails();
    }    

    public RestApplicationException(ApiErrorCode apiErrorCode, String details ) {
        super(details);
        this.apiErrorCode = apiErrorCode;
        this.httpStatus = apiErrorCode.getHttpStatus();
        this.appCode = apiErrorCode.getAppCode();
        this.details = details;  
        this.title = apiErrorCode.getTitle();
    }
    
    public ApiErrorCode getApiErrorCode() {
        return apiErrorCode;
    }

    public void setApiErrorCode(ApiErrorCode apiErrorCode) {
        this.apiErrorCode = apiErrorCode;
    }  

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    

}
