package joch.armada.data.redis.service.impl;

import joch.armada.data.redis.service.IRedisHashService;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author ic
 */
@Component
public class RedisHashServiceImpl implements IRedisHashService<Object> {

    private final int timeout = 600; // use 10 minutes
    private final int expiration = 60;
    private static final Logger logger = LoggerFactory.getLogger(RedisHashServiceImpl.class);

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public boolean hasKey(Object key) {
        return redisTemplate.hasKey(key);
    }

    @Override
    public void hAdd(Object hash, Object hashKey, Object value) {
        if (hasKey(hash)) {
            if (this.hExistsHashKey(hash, hashKey)) {
                redisTemplate.opsForHash().put(hash, hashKey, value);
            } else {
                hPutIfAbsent(hash, hashKey, value);
            }
        } else {
            this.hPutIfAbsent(hash, hashKey, value);
        }

    }

    @Override
    public void hAdd(Object hash, Object hashKey, Object value, long expirationInSeconds) {
        if (hasKey(hash)) {
            if (this.hExistsHashKey(hash, hashKey)) {
                redisTemplate.opsForHash().put(hash, hashKey, value);
            } else {
                hPutIfAbsent(hash, hashKey, value);
            }
        } else {
            hPutIfAbsent(hash, hashKey, value);
        }
        redisTemplate.expire(hash, expirationInSeconds, TimeUnit.SECONDS);
    }

    @Override
    public Object hAdded(Object hash, Object hashKey, Object value) {
        redisTemplate.opsForHash().put(hash, hashKey, value);
        return (Object) redisTemplate.opsForHash().get(hash, hashKey);
    }

    @Override
    public boolean hExistsHashKey(Object hash, Object hashKey) {
        return redisTemplate.opsForHash().hasKey(hash, hashKey);
    }

    @Override
    public void hPutIfAbsent(Object hash, Object hashKey, Object value) {
        redisTemplate.opsForHash().putIfAbsent(hash, hashKey, value);
    }

    @Override
    public void hPutIfAbsent(Object hash, Object hashKey, Object value, long expirationInSeconds) {
        redisTemplate.opsForHash().putIfAbsent(hash, hashKey, value);
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        Date date = Date.from(zonedDateTime.plus(expirationInSeconds, ChronoUnit.SECONDS).toInstant());
        redisTemplate.expireAt(hash, date);
    }

    @Override
    public Object hGet(Object hash, Object hashKey) {
        return redisTemplate.opsForHash().get(hash, hashKey);
    }

}
