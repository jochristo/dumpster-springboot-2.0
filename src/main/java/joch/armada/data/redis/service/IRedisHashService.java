package joch.armada.data.redis.service;

/**
 * Defines the caching operations for hashes on implementing redis service class.
 * Ops for hash methods.
 * @author ic
 * @param <T> The hash of cached object
 */
public interface IRedisHashService<T extends Object>
{        
    public boolean hasKey(Object key);    
    public void hAdd(Object hash, Object hashKey, T value);    
    public void hAdd(Object hash, Object hashKey, T value, long expiration);    
    public T hAdded(Object hash, Object hashKey, T value);
    boolean hExistsHashKey(Object hash, Object hashKey);    
    void hPutIfAbsent(Object hash, Object hashKey, T value);    
    public void hPutIfAbsent(Object hash, Object hashKey, T value, long expirationInSeconds);    
    public T hGet(Object hash, Object hashKey);    
}
