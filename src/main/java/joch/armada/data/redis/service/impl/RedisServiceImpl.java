package joch.armada.data.redis.service.impl;

import joch.armada.data.redis.service.IRedisService;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * Redis cache client implementation
 * @author ic
 */
@Service("redisService")
public class RedisServiceImpl implements IRedisService
{     
    private final int timeout = 10; // use 10 seconds
    private final int expiration = 60;    
    private static final Logger logger = LoggerFactory.getLogger(RedisServiceImpl.class);
        
    @Autowired    
    private RedisTemplate<String, Object> redisTemplate;    
        
    @Autowired
    public RedisServiceImpl(final RedisTemplate<String, Object> redisTemplate)
    {
        this.redisTemplate = redisTemplate;        
    }                

    @Override    
    public void setExpiry(int exp)
    {
        //logger.info("Setting redis expiration to " + exp + " seconds", cacheManager);
        //this.cacheManager.setDefaultExpiration(exp);
    }
    
    public RedisServiceImpl() {
    }
    
    
    
    @Override
    public void add(String key, Object value)
    {
        if(this.hasKey(key)){
            redisTemplate.opsForValue().set(key, value);
        }
        else
        {
            redisTemplate.opsForValue().setIfAbsent(key, value);
        }        
    }
    
    @Override
    public void add(String key, Object value, long expirationInSeconds)
    {                
        this.add(key, value);
        redisTemplate.expire(key, expirationInSeconds, TimeUnit.SECONDS);        
    }
    
    @Override
    public Object added(String key, Object value) {
        this.add(key, value);
        return get(key);
    }    
    
    @Override
    public Object added(String key, Object value, long expirationInSeconds)
    {
        this.add(key, value);
        redisTemplate.expire(key, expirationInSeconds, TimeUnit.SECONDS);          
        return get(key);
    }     
    
    @Override
    public void update(String key, Object value) {
        redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }
    
    @Override
    public Object updated(String key, Object value, long expirationInSeconds) {
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key, expirationInSeconds, TimeUnit.SECONDS);          
        return get(key);        
    }    
    
    
    @Override
    public Object get(String key)
    {
        return redisTemplate.opsForValue().get(key);
    }
    
    @Override
    public void delete(String key)
    {
        redisTemplate.delete(key);
    }


    @Override
    public Object updated(String key, Object value) {
        redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
        return get(key);
    }
    
    @Override
    public boolean hasKey(String key)
    {
        return redisTemplate.hasKey(key);
    }
    
    @Override
    public String randomKey()
    {
        return redisTemplate.randomKey();
    }
    
    @Override
    public long getExpire(String key)
    {
        return redisTemplate.getExpire(key);
    }

    @Override
    public void hAdd(String key, String field, Object value)
    {   
        if(hasKey(key))
        {        
            if(this.hExistsHashKey(key, field))
            {                
                redisTemplate.opsForHash().delete(field, value);
                hPutIfAbsent(key, field, value);
            }
            else
            {
                hPutIfAbsent(key, field, value);
            }
        }
        else
        {
            this.hPutIfAbsent(key, field, value);
        }
        
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        Date date = Date.from(zonedDateTime.plus(timeout, ChronoUnit.SECONDS).toInstant());        
        redisTemplate.expireAt(key, date);        
        
    }

    @Override
    public void hAdd(String key, String field, Object value, long expirationInSeconds) {
        if(this.hExistsHashKey(key, field))
        {
            redisTemplate.opsForHash().delete(field, value);
        }
        redisTemplate.opsForHash().put(key, field, value);        
        redisTemplate.expire(field, expirationInSeconds, TimeUnit.SECONDS);
    }

    @Override
    public Object hAdded(String key, String field, Object value) {
        redisTemplate.opsForHash().put(key, field, value);
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        Date date = Date.from(zonedDateTime.plus(timeout, ChronoUnit.SECONDS).toInstant());        
        redisTemplate.expireAt(field, date); 
        return (Object) redisTemplate.opsForHash().get(key, field);
    }    
    
    @Override
    public boolean hExistsHashKey(String key, String hashKey) {
        return redisTemplate.opsForHash().hasKey(key, hashKey);
    }

    @Override
    public void hPutIfAbsent(String type, String key, Object value)
    {
        redisTemplate.opsForHash().putIfAbsent(type, key, value);
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        Date date = Date.from(zonedDateTime.plus(timeout, ChronoUnit.SECONDS).toInstant());        
        redisTemplate.expireAt(key, date);            
    }

    @Override
    public void hPutIfAbsent(String type, String key, Object value, long expirationInSeconds)
    {
        redisTemplate.opsForHash().putIfAbsent(type, key, value);
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        Date date = Date.from(zonedDateTime.plus(expirationInSeconds, ChronoUnit.SECONDS).toInstant());        
        redisTemplate.expireAt(key, date);            
    }

    @Override
    public Object hGet(String h, String hashKey)
    {
        return (Object) redisTemplate.opsForHash().get(h, hashKey);
        
    }
    
}
