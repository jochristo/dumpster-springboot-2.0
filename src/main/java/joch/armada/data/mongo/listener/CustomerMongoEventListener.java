
package joch.armada.data.mongo.listener;

import joch.armada.api.error.ApiErrorCode;
import joch.armada.core.exception.DataConstraintViolationException;
import joch.armada.api.domain.Customer;
import joch.armada.data.mongo.service.IMongoDbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;

/**
 *
 * @author ic
 */
public class CustomerMongoEventListener extends AbstractMongoEventListener<Customer>
{
    @Autowired
    private IMongoDbService<Customer> service;

    @Override
    public void onBeforeSave(BeforeSaveEvent<Customer> event) {
        // check business rules, duplicates, etc
        String email = event.getSource().getEmail();
        if(service.findOne("email", email) != null) throw new DataConstraintViolationException(ApiErrorCode.DATA_INTEGRITY_VIOLATION, "Customer email exists and it would create duplicate entries");
        
    }

    
}
