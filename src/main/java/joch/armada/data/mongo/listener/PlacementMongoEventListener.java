package joch.armada.data.mongo.listener;

import java.util.List;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.core.exception.ApplicationConstraintException;
import joch.armada.api.domain.Dumpster;
import joch.armada.api.domain.Placement;
import joch.armada.api.service.IDumpsterService;
import joch.armada.api.service.IPlacementService;
import joch.armada.api.service.IReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;

/**
 *
 * @author ic
 */
public class PlacementMongoEventListener extends AbstractMongoEventListener<Placement>
{
    @Autowired private IDumpsterService dumpsterService;    
    @Autowired private IReservationService reservationService; 
    @Autowired private IPlacementService placementService; 
    @Autowired private MongoTemplate mongoTemplate;

    @Override
    public void onBeforeSave(BeforeSaveEvent<Placement> event)
    {
        // check availability prior to new reservation/placement
        List<Dumpster> data = dumpsterService.get();
        if(data.isEmpty() == false){
            Dumpster dumpster = data.get(0);
            if(dumpster.getAvailable() == 0){
                throw new ApplicationConstraintException(null,"Dumpster allocation is not possible due to unavailability");
            }            
        }
        else
        {
            // dumpster info has not been initialized
            throw new ApplicationConstraintException(ApiErrorCode.CONSTRAINT_VIOLATION, "Dumpster information is not available, system has not been initialized");            
        }
    }    

    @Override
    public void onBeforeDelete(BeforeDeleteEvent<Placement> event) {
        String id = (String) event.getSource().get("id");                
        Placement placement = placementService.findOne(id);
        placementService.hasReservationDependency(placement.getId());
    }
    
    
}
