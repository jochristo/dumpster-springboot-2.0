package joch.armada.data.mongo.listener;

import java.util.List;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.core.exception.ApplicationConstraintException;
import joch.armada.api.domain.Dumpster;
import joch.armada.api.domain.Placement;
import joch.armada.api.domain.Reservation;
import joch.armada.api.service.IDumpsterService;
import joch.armada.api.service.IPlacementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;

/**
 *
 * @author ic
 */
public class ReservationMongoEventListener extends AbstractMongoEventListener<Reservation>
{
    @Autowired private IDumpsterService dumpsterService;    
    @Autowired private IPlacementService placementService;    

    @Override
    public void onBeforeSave(BeforeSaveEvent<Reservation> event)
    {
        // check availability prior to new reservation/placement
        List<Dumpster> data = dumpsterService.get();
        if(data.isEmpty() == false){
            Dumpster dumpster = data.get(0);
            if(dumpster.getAvailable() == 0){
                throw new ApplicationConstraintException( null, "Dumpster reservation and allocation is not possible due to unavailability");
            }
            
            List<Placement> placements = event.getSource().getPlacements();
            if(placements != null){
                int available = dumpster.getAvailable();
                if(placements.size() > available){
                    
                    //delete stored placements
                placements.forEach(pl->
                {                    
                    placementService.delete(pl);
                });                    
                    throw new ApplicationConstraintException(ApiErrorCode.CONSTRAINT_VIOLATION, "Cannot allocate dumpster resource(s) due to unavailability. Available: "+ available); 
                }
                
                
                /*
                // save placements
                List<Placement> listOfPlacements = new ArrayList();
                placements.forEach(pl->
                {                    
                    //placementService.save(pl);
                    listOfPlacements.add(pl);                    
                });
                
                event.getSource().setPlacements(listOfPlacements);
                String debug = "debug";
                */
                
            }
        }
        else
        {
            // dumpster info has not been initialized
            throw new ApplicationConstraintException(ApiErrorCode.CONSTRAINT_VIOLATION, "Dumpster information is not available, system  has not been initialized");            
        }
    }   
    
}
