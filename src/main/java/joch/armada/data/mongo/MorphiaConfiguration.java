package joch.armada.data.mongo;

import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.filter.AnnotationTypeFilter;

/**
 * MongoDB Morphia auto configuration class.
 * @author ic
 */
@Configuration
public class MorphiaConfiguration
{   
    @Value("${morphia.entity.basepackage}")
    private String basePackage;
    
    @Value("${morphia.datastore.name}")
    private String dbName;    
    
    @Autowired
    private  MongoClient mongoClient;
    
    @Bean
    public Datastore datastore() throws ClassNotFoundException {
        Morphia morphia = new Morphia();

        // map entities by scanning base domain package
        ClassPathScanningCandidateComponentProvider entityScanner = new ClassPathScanningCandidateComponentProvider(true);
        entityScanner.addIncludeFilter(new AnnotationTypeFilter(Entity.class));        
        for (BeanDefinition candidate : entityScanner.findCandidateComponents(basePackage))
        { 
            morphia.map(Class.forName(candidate.getBeanClassName()));
        }

        return morphia.createDatastore(mongoClient, dbName); // "dataStoreInstanceId" may come from properties?
    }    
    
}
