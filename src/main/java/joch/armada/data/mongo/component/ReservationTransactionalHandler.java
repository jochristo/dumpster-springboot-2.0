package joch.armada.data.mongo.component;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.core.exception.GeneralApplicationCoreException;
import joch.armada.api.domain.Customer;
import joch.armada.api.domain.Placement;
import joch.armada.api.domain.Reservation;
import joch.armada.data.mongo.service.IMongoDbService;
import joch.armada.data.redis.service.IRedisHashService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.stereotype.Service;

/**
 *
 * @author ic
 */
@Service
public class ReservationTransactionalHandler implements ITransactionalEntityHandler<Reservation>
{
    private static final Logger logger = LoggerFactory.getLogger(ReservationTransactionalHandler.class); 
    
    private HashSet<Class<?>> refClasses;
    private Reservation instance;
    
    @Autowired
    private IMongoDbService<Reservation> reservationService;
    
    @Autowired
    private IMongoDbService<Placement> placementService;

    @Autowired
    private IMongoDbService<Customer> customerService;
    
    @Autowired
    private IRedisHashService redis;    

    /**
     * Creates new Reservation document and attaches included data prior to persisting.
     * Wraps the process in a transactional mode and "rolls" back insertions of reference data if an exception occurs.
     * @param reservation
     * @param included
     * @return 
     */
    @Override
    public Reservation save(@NotNull @Valid Reservation reservation, @NotNull @NotEmpty Collection<Object> included)
    {
        instance = reservation;
        try {
            refClasses = getReferenceClasses(instance);
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            logger.error(ReservationTransactionalHandler.class.getName(), ex);
            throw new GeneralApplicationCoreException(ApiErrorCode.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
        
        List<Placement> placements = new ArrayList();
        
        // iterate over included data
        included.stream().forEach((item) -> {
            if (refClasses.contains(item.getClass()) == false) {
                throw new GeneralApplicationCoreException(ApiErrorCode.INTERNAL_SERVER_ERROR, "Wrong DBRef class");
            }

            if (item instanceof Customer)
            {
                Customer c = (Customer)item;                
                customerService.save((Customer) item);     
                // add to cache
                String hash = Customer.class.getSimpleName() + "/" + c.getId();
                redis.hAdd(hash, c.getId(), c, 10);    
                
                // ser customer ref
                instance.setCustomer(c);                
            }
            else if (item instanceof Placement)
            {
                Placement pl = (Placement) item;
                placementService.save(pl);
                // add to cache
                String hash = Placement.class.getSimpleName() + "/" + pl.getId();
                redis.hAdd(hash, pl.getId(), pl, 10);
                
                placements.add(pl);
            }

        });
        // set placements ref
        if(placements.isEmpty() == false){
            instance.setPlacements(placements);
        }
        
        try
        {             
            instance.newInstance();
            logger.info("Saving reservation document...");                        
            reservationService.save(instance);
            logger.info("Done.");        
            logger.info("Loading reservation document...");
            instance = reservationService.findOne("id", instance.getId());             
        }
        catch(Exception ex){
            placements.stream().forEachOrdered((pl)->
            {
                if(redis.hasKey("Placement/"+pl.getId())){
                    placementService.delete(pl);
                }
            });
            if (redis.hasKey("Customer/" + instance.getCustomer().getId())) {
                customerService.delete(instance.getCustomer());
            }
            throw new GeneralApplicationCoreException(ApiErrorCode.INTERNAL_SERVER_ERROR, ex.getMessage());
        }         
     
        return instance;
        
    }
    
    protected HashSet<Class<?>> getReferenceClasses(Object source) throws IllegalArgumentException, IllegalAccessException
    {
        HashSet<Class<?>> set = new HashSet();
        Field[] fields = source.getClass().getDeclaredFields();
        for(Field field : fields){
            if (field.isAnnotationPresent(DBRef.class)) {
                field.setAccessible(true);
                Object value;            
                value = field.get(source);                
                //check reference type: object/collection
                if(value != null) {
                    if(value instanceof Collection){
                        Collection valueCollection = (Collection) value;
                        List list = new ArrayList(valueCollection); // copy indirect list to new list
                        Object item = list.get(0);
                        set.add(item.getClass()); // add collection item's class to set
                    }
                    else {
                        set.add(value.getClass()); // add to set
                    }
                }                
            }
        }
        return set;
    }
    
}
