package joch.armada.data.mongo.predicate;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;
import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * @author ic
 */
public class TypePathResolver
{
    //private static Object value;
    
    public static BooleanExpression getPathExpression(Criterion criterion,PathBuilder entityPath)
    {
        if(criterion == null){
            throw new IllegalArgumentException("Criterion object is null");
        }
        if(entityPath == null){
            throw new IllegalArgumentException("PathBuilder object is null");
        }        
        
        Object key = criterion.getKey();
        Object value = criterion.getValue();
        PredicateOperation operation = criterion.getPredicateOperation();
        if(key == null){
            throw new IllegalArgumentException("Criterion key is null");
        }    
        if(value == null){
            throw new IllegalArgumentException("Criterion value is null");
        }      
        if(operation == null){
            throw new IllegalArgumentException("Criterion predicate operation is null");
        }           
        
        if (NumberUtils.isParsable(value.toString())) {
            NumberPath<Integer> path = entityPath.getNumber(key.toString(), Integer.class);
            int numericValue = Integer.parseInt(value.toString());
            switch (criterion.getOperation()) {
                case ":":
                    return path.eq(numericValue);
                case ">":
                    return path.goe(numericValue);
                case "<":
                    return path.loe(numericValue);
            }
        } 
        else {
            StringPath path = entityPath.getString(criterion.getKey());
            if (criterion.getOperation().equalsIgnoreCase(":")) {
                return path.eq(criterion.getValue().toString());
            }
        }        
        return null;
    }
    
    private static BooleanExpression resolveTypeExpression(PredicateOperation operation, Object value, Path path)
    {
        return null;
    }
    
}
