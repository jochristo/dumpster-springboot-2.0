package joch.armada.data.mongo.predicate;

import com.querydsl.core.types.dsl.BooleanExpression;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ic
 */
public class CustomerPredicateBuilder
{
    private List<Criterion> criteria;

    public CustomerPredicateBuilder()
    {
        this.criteria = new ArrayList();
    }
    
    public CustomerPredicateBuilder with(String key, String operation, Object value)
    {   
        criteria.add(new Criterion(key, operation, value));
        return this;
    }    
    
    public BooleanExpression build() {
        if (criteria.size() == 0) {
            return null;
        }
 
        List<BooleanExpression> predicates = new ArrayList<>();
        CustomerPredicate predicate;
        for (Criterion criterion : criteria) {
            predicate = new CustomerPredicate(criterion);
            BooleanExpression exp = predicate.getPredicate();
            if (exp != null) {
                predicates.add(exp);
            }
        }
 
        BooleanExpression result = predicates.get(0);
        for (int i = 1; i < predicates.size(); i++) {
            result = result.and(predicates.get(i));
        }
        return result;
    }    
}
