package joch.armada.data.mongo.predicate;

/**
 *
 * @author ic
 */
public class Criterion
{
    private String key;
    private String operation;
    private Object value;
    private PredicateOperation predicateOperation;
    private Object[] values; // between values

    public Criterion(String key, PredicateOperation predicateOperation, Object value) {
        this.key = key;
        this.value = value;
        this.predicateOperation = predicateOperation;
    }
    
    public Criterion(String key, String operation, Object value) {
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

    public Criterion(String key, PredicateOperation predicateOperation, Object value,  Object[] values) {
        this.value = value;
        this.key = key;
        this.predicateOperation = predicateOperation;
        this.values = values;
    }

    public Object[] getValues() {
        return values;
    }

    public void setValues(Object[] values) {
        this.values = values;
    }    
    
    public PredicateOperation getPredicateOperation() {
        return predicateOperation;
    }

    public void setPredicateOperation(PredicateOperation predicateOperation) {
        this.predicateOperation = predicateOperation;
    }    
    
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
    
    
}
