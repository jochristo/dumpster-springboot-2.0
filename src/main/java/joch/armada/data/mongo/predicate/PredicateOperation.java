package joch.armada.data.mongo.predicate;

/**
 * The type of operation provided to a Predicate.
 * @author ic
 */
public enum PredicateOperation
{
    EQUALS(0,"EQ"), 
    
    LESS_THAN(1,"LT"), 
    
    GREATER_THAN(2,"GT"),
    
    LESS_THAN_OR_EQUAL_TO(3,"LOE"), 
    
    GREATER_THAN_OR_EQUAL_TO(4,"GOE"), 
    
    BETWEEN(5,"BETWEEN"),
    
    NOT_BETWEEN(6,"NOT_BETWWEN"),
    
    CONTAINS(7,"CONTAINS"),
    
    CONTAINTS_IGNORE_CASE(8,"CONTAINS_IGNORE_CASE");
    
    private final int value;
    private final String description;
    private PredicateOperation(int value, String description){
        this.value = value;
        this.description = description;
    };

       
    public final String getDescription() {
        return description;
    }            
    
    
}
