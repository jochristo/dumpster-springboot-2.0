package joch.armada.data.mongo.predicate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import joch.armada.api.repository.IBaseMongoRepository;
import org.joko.core.utilities.Utilities;

/**
 *
 * @author ic
 * @param <T>
 */
public abstract class BaseQueryPredicateService<T extends Object>
{        
    private IBaseMongoRepository<T> repository;
    
    protected List<T> findWithMap(IBaseMongoRepository<T> repository, Map<Object,Object> requestMap)
    {
        if(Utilities.isEmpty(requestMap) == false)
        {
            String email = (String) requestMap.get("email");
            String mobileNo = (String) requestMap.get("mobileNo");
            String phoneNo = (String) requestMap.get("phoneNo");
            String lastname = (String) requestMap.get("lastname");
            
            // querydsl methods            
            EntityPredicateBuilder<T> predicate = new EntityPredicateBuilder();
            if(!Utilities.isEmpty(email)){
                predicate.with("email", ":", email);
            }
            if(!Utilities.isEmpty(mobileNo)){
                 predicate.with("mobileNo", ":", mobileNo);
            }      
            if(!Utilities.isEmpty(phoneNo)){
                 predicate.with("phoneNo", ":", phoneNo);
            } 
            if(!Utilities.isEmpty(lastname)){
                 predicate.with("lastname", ":", lastname);
            }             
            
            List<T> list = new ArrayList();
            Iterable<T> iterable = (Iterable<T>) repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;
            
        }
        return null;         
    }
        
    protected List<T> findWithMap(Map<Object,Object> requestMap)
    {
        if(Utilities.isEmpty(requestMap) == false)
        {
            String email = (String) requestMap.get("email");
            String mobileNo = (String) requestMap.get("mobileNo");
            String phoneNo = (String) requestMap.get("phoneNo");
            String lastname = (String) requestMap.get("lastname");
            
            // querydsl methods            
            EntityPredicateBuilder<T> predicate = new EntityPredicateBuilder();
            if(!Utilities.isEmpty(email)){
                predicate.with("email", ":", email);
            }
            if(!Utilities.isEmpty(mobileNo)){
                 predicate.with("mobileNo", ":", mobileNo);
            }      
            if(!Utilities.isEmpty(phoneNo)){
                 predicate.with("phoneNo", ":", phoneNo);
            } 
            if(!Utilities.isEmpty(lastname)){
                 predicate.with("lastname", ":", lastname);
            }             
            
            List<T> list = new ArrayList();
            Iterable<T> iterable = (Iterable<T>) repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;
            
        }
        return null;       
    }
    
    
}
