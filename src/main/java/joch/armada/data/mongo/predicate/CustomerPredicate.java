package joch.armada.data.mongo.predicate;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;
import org.apache.commons.lang3.math.NumberUtils;
import joch.armada.api.domain.Customer;


/**
 *
 * @author ic
 */
public class CustomerPredicate
{
    private Criterion criterion;

    public CustomerPredicate(Criterion criterion) {
        this.criterion = criterion;
    }
    
    public BooleanExpression getPredicate() {
        PathBuilder<Customer> entityPath = new PathBuilder<>(Customer.class, "customer");
 
        if (NumberUtils.isParsable(criterion.getValue().toString())) {
            NumberPath<Integer> path = entityPath.getNumber(criterion.getKey(), Integer.class);
            int value = Integer.parseInt(criterion.getValue().toString());
            switch (criterion.getOperation()) {
                case ":":
                    return path.eq(value);
                case ">":
                    return path.goe(value);
                case "<":
                    return path.loe(value);
            }
        } 
        else {
            StringPath path = entityPath.getString(criterion.getKey());
            if (criterion.getOperation().equalsIgnoreCase(":")) {
                return path.containsIgnoreCase(criterion.getValue().toString());
            }
        }
        return null;
    }    
    
}
