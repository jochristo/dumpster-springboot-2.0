package joch.armada.data.mongo.predicate;

import com.querydsl.core.types.dsl.BooleanExpression;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Generic entity predicate builder class to support many criteria.
 * @author ic
 * @param <T>
 */
public class EntityPredicateBuilder<T>
{
    private final List<Criterion> criteria;   
    private Class<T> _clazz;
    
    public EntityPredicateBuilder()
    {
        this.criteria = new ArrayList();                 
    }    
    
    public EntityPredicateBuilder(Class<T> clazz)
    {
        this.criteria = new ArrayList();      
        _clazz = clazz;
    }      
       
    public EntityPredicateBuilder with(String key, String operation, Object value)
    {
        if(value != null){
            criteria.add(new Criterion(key, operation, value));
        }
        return this;
    }    
    
    public EntityPredicateBuilder with(String key, PredicateOperation operation, Object value)
    {   
        if(value != null){
            criteria.add(new Criterion(key, operation, value));
        }
        return this;
    }     
    
    public EntityPredicateBuilder with(String key, PredicateOperation operation, Object value, Object[] values)
    {   
        if(value != null){
            criteria.add(new Criterion(key, operation, value, values));
        }
        return this;
    }     
    
    public BooleanExpression build() {
        if (criteria.size() == 0) {
            return null;
        }
 
        List<BooleanExpression> predicates = new ArrayList<>();        
        EntityPredicate<T> predicate;
        for (Criterion criterion : criteria) {            
            predicate = new EntityPredicate(criterion,_clazz);
            //BooleanExpression exp = predicate.getPredicate();
            BooleanExpression exp = predicate.getPredicateExpression();
            if (exp != null) {
                predicates.add(exp);
            }
        }
 
        BooleanExpression result = predicates.get(0);
        for (int i = 1; i < predicates.size(); i++) {
            result = result.and(predicates.get(i));
        }
        return result;
    }     
    
    public static Object[] determineValue(Object key, Object value)
    {
        if(key == null){
            throw new IllegalArgumentException("Key is null");
        }
        if(value == null){
            throw new IllegalArgumentException("Key's value is null");
        }    
        
        // no single value
        if((value instanceof HashMap))
        {
            HashMap<Object, Object> map = (HashMap<Object, Object>) value;
             
            // try read min amd max keys...
            Object min = map.get("min");
            Object max = map.get("max");
            Object[] values = new Object[2];
            values[0] = min;
            values[1] = max;
            return values;             
        }
        
        // return as is
        return null;
        
    }    
    
}
