package joch.armada.data.mongo.predicate;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Generic MongoDb entity predicate class to support criteria.
 * @author ic
 * @param <T>
 */
public class EntityPredicate<T>
{
    private final Criterion criterion;
    private Class<T> entityClass;
    
    public EntityPredicate(Criterion criterion)
    {
        this.criterion = criterion;        
                
        // get type T class
        Type type = this.getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType)type;        
        this.entityClass = (Class)pt.getActualTypeArguments()[0];
    }   
    
    public EntityPredicate(Criterion criterion, Class<T> clazz)
    {
        this.criterion = criterion;              
        this.entityClass = clazz;
    }        
        
    public BooleanExpression getPredicate() {
        
        PathBuilder entityPath = new PathBuilder<>(getEntityClass(), getEntityClass().getSimpleName());
 
        if (NumberUtils.isParsable(criterion.getValue().toString())) {
            NumberPath<Integer> path = entityPath.getNumber(criterion.getKey(), Integer.class);
            int value = Integer.parseInt(criterion.getValue().toString());
            switch (criterion.getOperation()) {
                case ":":
                    return path.eq(value);
                case ">":
                    return path.goe(value);
                case "<":
                    return path.loe(value);
            }
        } 
        else {
            StringPath path = entityPath.getString(criterion.getKey());
            if (criterion.getOperation().equalsIgnoreCase(":")) {
                return path.eq(criterion.getValue().toString());
            }
        }
        return null;
    }      
    
    public BooleanExpression getPredicateExpression() {
        
        PathBuilder entityPath = new PathBuilder<>(getEntityClass(), getEntityClass().getSimpleName());
        
        Object[] values = criterion.getValues();
                 
        if (NumberUtils.isParsable(criterion.getValue().toString())) {
            NumberPath<Integer> path = entityPath.getNumber(criterion.getKey(), Integer.class);
            int value = Integer.parseInt(criterion.getValue().toString());           
                        
            switch (criterion.getPredicateOperation())
            {
                case EQUALS:
                    return path.eq(value);
                case GREATER_THAN_OR_EQUAL_TO:
                    return path.goe(value);
                case LESS_THAN_OR_EQUAL_TO:
                    return path.loe(value);
                case GREATER_THAN:
                    return path.gt(value);
                case LESS_THAN:
                    return path.lt(value);                                              
            }
        } 
        else if(values != null)
        {
            NumberPath<Integer> path = entityPath.getNumber(criterion.getKey(), Integer.class);
            switch (criterion.getPredicateOperation())
            {
                // to do
                case BETWEEN:
                    if(values == null){
                        throw new IllegalArgumentException("Criterion does not contain values to compare against an IN BETWEEN expression");
                    }                    
                    int min = Integer.parseInt(values[0].toString());
                    int max = Integer.parseInt(values[1].toString());
                    return path.between(min, max);   
                // to do
                case NOT_BETWEEN:
                    if(values == null){
                        throw new IllegalArgumentException("Criterion does not contain values to compare against a NOT BETWEEN expression");
                    }                    
                    min = Integer.parseInt(values[0].toString());
                    max = Integer.parseInt(values[1].toString());
                    return path.between(min, max);                 
            }                                    
        }
        else {
            StringPath path = entityPath.getString(criterion.getKey());
            String value = criterion.getValue().toString();
            switch (criterion.getPredicateOperation())
            {
                case EQUALS:
                    return path.eq(value);
                case CONTAINS:
                    return path.contains(value);
                case CONTAINTS_IGNORE_CASE:
                    return path.containsIgnoreCase(value);                
            }
        }
        return null;
    }     

    protected Class<T> getEntityClass() {
        return entityClass;
    }
    
}
