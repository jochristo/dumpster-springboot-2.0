package joch.armada.data.mongo.service;

import java.util.List;
import org.mongodb.morphia.Datastore;

/**
 * Defines methods on MongoDB domain entities.
 * @author ic
 */
public interface IMorphiaService
{
    public <T extends Object> T save(T object);
    
    public <T extends Object> T update(T object);
    
    public <T extends Object> void delete(T object);
    
    public <T extends Object> T get(T object, Object id);
    
    public <T extends Object> List<T> get();
    
    public Datastore datastore();
}
