package joch.armada.data.mongo.service;

import java.util.List;
import java.util.Map;
import joch.armada.data.mongo.predicate.PredicateOperation;

/**
 * Defines methods to operate on MongoDb entities-collections.
 * @author ic
 * @param <T> The type of the collection stored.
 */
public abstract interface IMongoDbService<T extends Object>
{
    public List<T> get();
    
    public T findOne(String id);
    
    public T save(T object);
    
    public T update(Object id, T object);
    
    public void delete(T object);
    
    public void insert(T object);        
        
    public T findOne(Object object);
    
    public T findOne(String key, Object object);
    
    public List<T> get(Object object);
    
    public List<T> find(Map<Object,Object> requestMap);
    
    public List<T> find(Map<Object,Object> requestMap, PredicateOperation operation);

}
