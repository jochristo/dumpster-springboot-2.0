package joch.armada.data.mongo.service.impl;

import java.util.List;
import joch.armada.data.mongo.service.IMorphiaService;
import org.mongodb.morphia.Datastore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ic
 */
@Service
public class MorphiaService implements IMorphiaService
{    
    @Autowired
    private Datastore mongoDatastore;
    
    @Override
    public Object save(Object object)
    {
        return mongoDatastore.save(object);        
    }
    
    @Override
    public Datastore datastore()
    {
        return this.mongoDatastore;
    }

    @Override
    public <T> T update(T object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> void delete(T object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> T get(T object, Object id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> List<T> get() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    
}
