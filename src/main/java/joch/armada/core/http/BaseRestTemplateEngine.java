package joch.armada.core.http;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * RestTemplate HTTP requests engine
 * @author Admin
 */
@Component
public class BaseRestTemplateEngine
{       
    private final RestTemplate rest;
    private final HttpHeaders headers;
    private HttpStatus status;    
    //private Class<?> clazz;
        
    /***
     * Initializes a new instance of REST operations engine with default parameters. 
    */
    public BaseRestTemplateEngine()
    {
        this.rest = new RestTemplate();
        this.headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");  
        
        // set class type
        //Type type = getClass().getGenericSuperclass();
        //ParameterizedType pt = (ParameterizedType)type;
        //clazz = (Class)pt.getActualTypeArguments()[0];        
    }    
    
    /**
     * Sends a GET request to given URI and returns data.
     * @param <T>
     * @param uri
     * @param clazz
     * @return 
     */
    public <T extends Object> T get(String uri, Class<T> clazz)
    {        
      HttpEntity<T> requestEntity = new HttpEntity<>(null, headers);
      ResponseEntity<T> responseEntity = rest.exchange(uri, HttpMethod.GET, requestEntity, clazz);
      this.setStatus(responseEntity.getStatusCode());
      return responseEntity.getBody();
    }

    /**
     * Sends a POST request to given URI with given body document data of type T.
     * @param <T>
     * @param uri
     * @param data
     * @param clazz
     * @return 
     */
    public <T extends Object> T post(String uri, T data, Class<T> clazz)
    {           
        HttpEntity<T> requestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<T> responseEntity = rest.exchange(uri, HttpMethod.POST, requestEntity, clazz);
        this.setStatus(responseEntity.getStatusCode());
        return responseEntity.getBody();
    }

    /**
     * Send a PUT request to given URI with given body document data of type T.
     * @param <T>
     * @param uri
     * @param data 
     * @param clazz 
     */
    public <T extends Object> void put(String uri, T data, Class<T> clazz)
    {               
        HttpEntity<T> requestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<T> responseEntity = rest.exchange(uri, HttpMethod.PUT, requestEntity, clazz);
        this.setStatus(responseEntity.getStatusCode());   
    }
    
    
    /**
     * 
     * @param <T>
     * @param uri
     * @param data
     * @param clazz 
     */
    public <T extends Object> void delete(String uri, T data, Class<T> clazz)
    {               
        HttpEntity<T> requestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<T> responseEntity = rest.exchange(uri, HttpMethod.DELETE, requestEntity, clazz);
        this.setStatus(responseEntity.getStatusCode());   
    }    

    public HttpStatus getStatus() {
      return status;
    }

    public void setStatus(HttpStatus status) {
      this.status = status;
    }    

  
}
