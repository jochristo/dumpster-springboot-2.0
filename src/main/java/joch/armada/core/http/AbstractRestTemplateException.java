package joch.armada.core.http;

import joch.armada.api.error.ApiError;
import joch.armada.api.error.ApiErrorCode;
import joch.armada.api.exception.RestApplicationException;
import org.springframework.http.HttpStatus;

/**
 *
 * @author ic
 */
public abstract class AbstractRestTemplateException extends RestApplicationException
{
    public AbstractRestTemplateException(String details) {
        super(details);
        super.setApiErrorCode(ApiErrorCode.RESOURCE_NOT_FOUND);
    }

    public AbstractRestTemplateException(ApiError apiError, String details) {
        super(apiError, details);        
    }

    public AbstractRestTemplateException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
        
    }
    
    public AbstractRestTemplateException(HttpStatus httpStatus, ApiError apiError, String details) {
        super(details);
        this.appCode = apiError.getAppCode();
        this.details = details;
        this.title = apiError.getDetails();
        this.httpStatus = httpStatus;
    }    
}
