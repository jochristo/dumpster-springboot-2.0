package joch.armada.core.http;

/**
 *
 * @author ic
 * @param <T>
 */
public interface IBaseRestTemplateEngine<T extends Object>
{
    public T get(String uri);

    public T post(String uri, T data);

    public void put(String uri, T data);
    
}
