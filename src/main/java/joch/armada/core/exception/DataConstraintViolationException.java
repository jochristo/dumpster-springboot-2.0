package joch.armada.core.exception;

import joch.armada.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class DataConstraintViolationException extends AbstractCoreApplicationException
{
    
    public DataConstraintViolationException(ApiErrorCode apiErrorCode, String details) {
        super(ApiErrorCode.DATA_INTEGRITY_VIOLATION, details);       
    }
    
}
