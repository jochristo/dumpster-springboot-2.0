package joch.armada.core.exception;

import joch.armada.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class ApplicationConstraintException extends AbstractCoreApplicationException
{
    public ApplicationConstraintException(ApiErrorCode apiErrorCode, String details) {
        super(ApiErrorCode.CONSTRAINT_VIOLATION, details);
    }    
    
}
