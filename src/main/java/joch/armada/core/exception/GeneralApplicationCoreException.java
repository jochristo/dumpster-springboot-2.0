package joch.armada.core.exception;

import joch.armada.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class GeneralApplicationCoreException extends AbstractCoreApplicationException
{
    
    public GeneralApplicationCoreException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }
    
}
