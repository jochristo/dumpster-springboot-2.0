package joch.armada.core.exception;

import joch.armada.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public abstract class AbstractCoreApplicationException extends RuntimeException
{    
    private String code;
    
    private ApiErrorCode apiErrorCode;
    
    private String details;    
    
    private String title;   

    public AbstractCoreApplicationException(String details, String title) {
        super(details);
        this.details = details;
        this.title = title;
    }

    public AbstractCoreApplicationException(ApiErrorCode apiErrorCode, String details) {
        super(details);
        this.apiErrorCode = apiErrorCode;
        this.details = details;
        this.title = this.apiErrorCode.getTitle();
        this.code = this.apiErrorCode.getAppCode();        
    }    
        

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ApiErrorCode getApiErrorCode() {
        return apiErrorCode;
    }

    public void setApiErrorCode(ApiErrorCode apiErrorCode) {
        this.apiErrorCode = apiErrorCode;
    }    
        
    
    
}
